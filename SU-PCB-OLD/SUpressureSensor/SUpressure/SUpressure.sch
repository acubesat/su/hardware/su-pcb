EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SU pressure sensors PCB"
Date "2022-07-07"
Rev "0"
Comp "SpaceDot - AcubeSAT"
Comment1 "Bountzioukas Panagiotis"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x04_Counter_Clockwise J1
U 1 1 62C6E7E2
P 4350 4300
F 0 "J1" V 4446 4012 50  0000 R CNN
F 1 "Sensor" V 4355 4012 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W10.16mm" H 4350 4300 50  0001 C CNN
F 3 "~" H 4350 4300 50  0001 C CNN
	1    4350 4300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 62C7085F
P 4250 4700
F 0 "#PWR01" H 4250 4450 50  0001 C CNN
F 1 "GND" H 4255 4527 50  0000 C CNN
F 2 "" H 4250 4700 50  0001 C CNN
F 3 "" H 4250 4700 50  0001 C CNN
	1    4250 4700
	1    0    0    -1  
$EndComp
Text Label 4350 4500 3    50   ~ 0
Vout1+
Wire Wire Line
	4250 4700 4250 4500
Text Label 4450 4500 3    50   ~ 0
Vcc
Text Label 4550 4500 3    50   ~ 0
Vout1-
NoConn ~ 4250 4000
NoConn ~ 4350 4000
NoConn ~ 4450 4000
NoConn ~ 4550 4000
Text Notes 3800 4300 0    50   Italic 0
Bard Side
$Comp
L Connector_Generic:Conn_02x04_Counter_Clockwise J3
U 1 1 62C756D2
P 5500 4300
F 0 "J3" V 5596 4012 50  0000 R CNN
F 1 "Sensor" V 5505 4012 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W10.16mm" H 5500 4300 50  0001 C CNN
F 3 "~" H 5500 4300 50  0001 C CNN
	1    5500 4300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 62C756D8
P 5400 4700
F 0 "#PWR02" H 5400 4450 50  0001 C CNN
F 1 "GND" H 5405 4527 50  0000 C CNN
F 2 "" H 5400 4700 50  0001 C CNN
F 3 "" H 5400 4700 50  0001 C CNN
	1    5400 4700
	1    0    0    -1  
$EndComp
Text Label 5500 4500 3    50   ~ 0
Vout2+
Wire Wire Line
	5400 4700 5400 4500
Text Label 5600 4500 3    50   ~ 0
Vcc
Text Label 5700 4500 3    50   ~ 0
Vout2-
NoConn ~ 5400 4000
NoConn ~ 5500 4000
NoConn ~ 5600 4000
NoConn ~ 5700 4000
Text Notes 4950 4300 0    50   Italic 0
Bard Side
$Comp
L SU_connectors:504050-0691 J2
U 1 1 62C75ACE
P 4600 5350
F 0 "J2" H 5000 5615 50  0000 C CNN
F 1 "504050-0691" H 5000 5524 50  0000 C CNN
F 2 "SU_connectors:504050-0691_2" H 5250 5450 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/504050-0691.pdfhttp://www.molex.com/pdm_docs/sd/5040500691_sd.pdf" H 5250 5350 50  0001 L CNN
F 4 "Pico-Lock SMT Wire-to-Board Header 6 way Molex Pico-Lock Series, Series Number 504050, 1.5mm Pitch 6 Way 1 Row Shrouded Right Angle PCB Header, Surface Mount" H 5250 5250 50  0001 L CNN "Description"
F 5 "538-504050-0691" H 5250 5050 50  0001 L CNN "Mouser Part Number"
F 6 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0691?qs=bvCPb%252BE7ys2K1LQC9e%2FvRg%3D%3D" H 5250 4950 50  0001 L CNN "Mouser Price/Stock"
F 7 "Molex" H 5250 4850 50  0001 L CNN "Manufacturer_Name"
F 8 "504050-0691" H 5250 4750 50  0001 L CNN "Manufacturer_Part_Number"
	1    4600 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 62C782AB
P 5400 5550
F 0 "#PWR03" H 5400 5300 50  0001 C CNN
F 1 "GND" H 5405 5377 50  0000 C CNN
F 2 "" H 5400 5550 50  0001 C CNN
F 3 "" H 5400 5550 50  0001 C CNN
	1    5400 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 5550 5400 5450
Connection ~ 5400 5550
Connection ~ 5400 5450
Wire Wire Line
	5400 5450 5400 5350
Text Label 4600 5550 2    50   ~ 0
Vcc
Text Label 4600 5450 2    50   ~ 0
Vout1+
Text Label 4600 5350 2    50   ~ 0
Vout2+
$EndSCHEMATC
