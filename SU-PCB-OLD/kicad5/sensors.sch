EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Components:LPS22HHTR IC?
U 1 1 5FBBA821
P 5150 1850
AR Path="/5FBBA821" Ref="IC?"  Part="1" 
AR Path="/5FB89B52/5FBBA821" Ref="IC2"  Part="1" 
F 0 "IC2" H 6150 2115 50  0000 C CNN
F 1 "LPS22HDTR" H 6150 2024 50  0000 C CNN
F 2 "Components:LPS22HHTR" H 7000 1950 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/lps22hhtr/stmicroelectronics" H 7000 1850 50  0001 L CNN
F 4 "STMICROELECTRONICS - LPS22HHTR - PRESSURE SENSOR, HLGA-10" H 7000 1750 50  0001 L CNN "Description"
F 5 "0.8" H 7000 1650 50  0001 L CNN "Height"
F 6 "STMicroelectronics" H 7000 1550 50  0001 L CNN "Manufacturer_Name"
F 7 "LPS22HHTR" H 7000 1450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "511-LPS22HHTR" H 7000 1350 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=511-LPS22HHTR" H 7000 1250 50  0001 L CNN "Mouser Price/Stock"
	1    5150 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA828
P 5150 2050
AR Path="/5FBBA828" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA828" Ref="#PWR042"  Part="1" 
F 0 "#PWR042" H 5150 1800 50  0001 C CNN
F 1 "GND" V 5155 1922 50  0000 R CNN
F 2 "" H 5150 2050 50  0001 C CNN
F 3 "" H 5150 2050 50  0001 C CNN
	1    5150 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 2150 5150 2150
Wire Wire Line
	4750 1950 5150 1950
Wire Wire Line
	5150 2250 4750 2250
$Comp
L power:GND #PWR?
U 1 1 5FBBA835
P 7150 1950
AR Path="/5FBBA835" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA835" Ref="#PWR044"  Part="1" 
F 0 "#PWR044" H 7150 1700 50  0001 C CNN
F 1 "GND" V 7155 1822 50  0000 R CNN
F 2 "" H 7150 1950 50  0001 C CNN
F 3 "" H 7150 1950 50  0001 C CNN
	1    7150 1950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA83B
P 7150 2050
AR Path="/5FBBA83B" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA83B" Ref="#PWR045"  Part="1" 
F 0 "#PWR045" H 7150 1800 50  0001 C CNN
F 1 "GND" V 7155 1922 50  0000 R CNN
F 2 "" H 7150 2050 50  0001 C CNN
F 3 "" H 7150 2050 50  0001 C CNN
	1    7150 2050
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA841
P 7150 2150
AR Path="/5FBBA841" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA841" Ref="#PWR046"  Part="1" 
F 0 "#PWR046" H 7150 1900 50  0001 C CNN
F 1 "GND" V 7155 2022 50  0000 R CNN
F 2 "" H 7150 2150 50  0001 C CNN
F 3 "" H 7150 2150 50  0001 C CNN
	1    7150 2150
	0    -1   -1   0   
$EndComp
Wire Notes Line
	7850 2550 7850 900 
Wire Notes Line
	7850 900  4100 900 
Wire Notes Line
	4100 900  4100 2550
Wire Notes Line
	4100 2550 7850 2550
$Comp
L Components:0.1u C?
U 1 1 5FBBA851
P 5150 1350
AR Path="/5FBBA851" Ref="C?"  Part="1" 
AR Path="/5FB89B52/5FBBA851" Ref="C3"  Part="1" 
F 0 "C3" V 5354 1478 50  0000 L CNN
F 1 "0.1u" V 5445 1478 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5500 1400 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/GRM022R61A104ME01L.pdf" H 5500 1300 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT" H 5500 1200 50  0001 L CNN "Description"
F 5 "0.22" H 5500 1100 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 5500 1000 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM022R61A104ME01L" H 5500 900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM022R61A104ME1L" H 5500 800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM022R61A104ME1L" H 5500 700 50  0001 L CNN "Mouser Price/Stock"
	1    5150 1350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA857
P 5150 1350
AR Path="/5FBBA857" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA857" Ref="#PWR040"  Part="1" 
F 0 "#PWR040" H 5150 1100 50  0001 C CNN
F 1 "GND" H 5155 1177 50  0000 C CNN
F 2 "" H 5150 1350 50  0001 C CNN
F 3 "" H 5150 1350 50  0001 C CNN
	1    5150 1350
	-1   0    0    1   
$EndComp
Text Notes 5650 900  0    50   ~ 0
Pressure Sensor #1
$Comp
L Components:SHT31-DIS-F2.5kS HM?
U 1 1 5FBBA864
P 1550 3050
AR Path="/5FBBA864" Ref="HM?"  Part="1" 
AR Path="/5FB89B52/5FBBA864" Ref="HM2"  Part="1" 
F 0 "HM2" H 2250 3315 50  0000 C CNN
F 1 "SHT30-DIS-F2.5kS" H 2250 3224 50  0000 C CNN
F 2 "Components:SON50P250X250X90-9N-D" H 2800 3150 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/SHT31-DIS-F2.5kS.pdf" H 2800 3050 50  0001 L CNN
F 4 "Board Mount Humidity Sensors SHT3x Humidity and Temperature Sensor" H 2800 2950 50  0001 L CNN "Description"
F 5 "0.9" H 2800 2850 50  0001 L CNN "Height"
F 6 "Sensirion" H 2800 2750 50  0001 L CNN "Manufacturer_Name"
F 7 "SHT31-DIS-F2.5kS" H 2800 2650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "403-SHT31-DIS-F" H 2800 2550 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=403-SHT31-DIS-F" H 2800 2450 50  0001 L CNN "Mouser Price/Stock"
	1    1550 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 3050 1550 3050
NoConn ~ 1550 3250
Wire Wire Line
	1300 3350 1550 3350
Wire Wire Line
	3350 3250 3300 3250
$Comp
L power:GND #PWR?
U 1 1 5FBBA871
P 2950 3050
AR Path="/5FBBA871" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA871" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 2950 2800 50  0001 C CNN
F 1 "GND" V 2955 2922 50  0000 R CNN
F 2 "" H 2950 3050 50  0001 C CNN
F 3 "" H 2950 3050 50  0001 C CNN
	1    2950 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 3150 2950 3050
Connection ~ 2950 3050
$Comp
L power:GND #PWR?
U 1 1 5FBBA879
P 2250 4050
AR Path="/5FBBA879" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA879" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 2250 3800 50  0001 C CNN
F 1 "GND" H 2255 3877 50  0000 C CNN
F 2 "" H 2250 4050 50  0001 C CNN
F 3 "" H 2250 4050 50  0001 C CNN
	1    2250 4050
	1    0    0    -1  
$EndComp
Wire Notes Line
	600  2700 600  4350
Wire Notes Line
	600  4350 3950 4350
Wire Notes Line
	3950 4350 3950 2700
Wire Notes Line
	3950 2700 600  2700
Text Notes 2000 2700 0    50   ~ 0
Humidity Sensor #2
$Comp
L power:GND #PWR?
U 1 1 5FBBA884
P 10200 2150
AR Path="/5FBBA884" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA884" Ref="#PWR053"  Part="1" 
F 0 "#PWR053" H 10200 1900 50  0001 C CNN
F 1 "GND" V 10205 2022 50  0000 R CNN
F 2 "" H 10200 2150 50  0001 C CNN
F 3 "" H 10200 2150 50  0001 C CNN
	1    10200 2150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA88A
P 10200 2250
AR Path="/5FBBA88A" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA88A" Ref="#PWR054"  Part="1" 
F 0 "#PWR054" H 10200 2000 50  0001 C CNN
F 1 "GND" V 10205 2122 50  0000 R CNN
F 2 "" H 10200 2250 50  0001 C CNN
F 3 "" H 10200 2250 50  0001 C CNN
	1    10200 2250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA890
P 9100 2250
AR Path="/5FBBA890" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA890" Ref="#PWR050"  Part="1" 
F 0 "#PWR050" H 9100 2000 50  0001 C CNN
F 1 "GND" V 9105 2122 50  0000 R CNN
F 2 "" H 9100 2250 50  0001 C CNN
F 3 "" H 9100 2250 50  0001 C CNN
	1    9100 2250
	0    1    1    0   
$EndComp
$Comp
L Components:0.1u C?
U 1 1 5FBBA89C
P 2950 3350
AR Path="/5FBBA89C" Ref="C?"  Part="1" 
AR Path="/5FB89B52/5FBBA89C" Ref="C2"  Part="1" 
F 0 "C2" V 3154 3478 50  0000 L CNN
F 1 "0.1u" V 3245 3478 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3300 3400 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/GRM022R61A104ME01L.pdf" H 3300 3300 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT" H 3300 3200 50  0001 L CNN "Description"
F 5 "0.22" H 3300 3100 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 3300 3000 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM022R61A104ME01L" H 3300 2900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM022R61A104ME1L" H 3300 2800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM022R61A104ME1L" H 3300 2700 50  0001 L CNN "Mouser Price/Stock"
	1    2950 3350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA8A2
P 2950 3850
AR Path="/5FBBA8A2" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA8A2" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 2950 3600 50  0001 C CNN
F 1 "GND" H 2955 3677 50  0000 C CNN
F 2 "" H 2950 3850 50  0001 C CNN
F 3 "" H 2950 3850 50  0001 C CNN
	1    2950 3850
	1    0    0    -1  
$EndComp
Wire Notes Line
	8000 800  11050 800 
Wire Notes Line
	11050 800  11050 2550
Wire Notes Line
	11050 2550 8000 2550
Wire Notes Line
	8000 2550 8000 800 
Text Notes 8700 800  0    50   ~ 0
Temperature Sensor #1 (I2C ADDR 000)
$Comp
L Components:SHT31-DIS-F2.5kS HM?
U 1 1 5FBBA8B3
P 1550 1250
AR Path="/5FBBA8B3" Ref="HM?"  Part="1" 
AR Path="/5FB89B52/5FBBA8B3" Ref="HM1"  Part="1" 
F 0 "HM1" H 2250 1515 50  0000 C CNN
F 1 "SHT30-DIS-F2.5kS" H 2250 1424 50  0000 C CNN
F 2 "Components:SON50P250X250X90-9N-D" H 2800 1350 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/SHT31-DIS-F2.5kS.pdf" H 2800 1250 50  0001 L CNN
F 4 "Board Mount Humidity Sensors SHT3x Humidity and Temperature Sensor" H 2800 1150 50  0001 L CNN "Description"
F 5 "0.9" H 2800 1050 50  0001 L CNN "Height"
F 6 "Sensirion" H 2800 950 50  0001 L CNN "Manufacturer_Name"
F 7 "SHT31-DIS-F2.5kS" H 2800 850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "403-SHT31-DIS-F" H 2800 750 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=403-SHT31-DIS-F" H 2800 650 50  0001 L CNN "Mouser Price/Stock"
	1    1550 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 1250 1550 1250
$Comp
L power:GND #PWR?
U 1 1 5FBBA8BB
P 1550 1350
AR Path="/5FBBA8BB" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA8BB" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 1550 1100 50  0001 C CNN
F 1 "GND" V 1555 1222 50  0000 R CNN
F 2 "" H 1550 1350 50  0001 C CNN
F 3 "" H 1550 1350 50  0001 C CNN
	1    1550 1350
	0    1    1    0   
$EndComp
NoConn ~ 1550 1450
Wire Wire Line
	1300 1550 1550 1550
Wire Wire Line
	3350 1450 3300 1450
$Comp
L power:GND #PWR?
U 1 1 5FBBA8C6
P 2950 1250
AR Path="/5FBBA8C6" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA8C6" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 2950 1000 50  0001 C CNN
F 1 "GND" V 2955 1122 50  0000 R CNN
F 2 "" H 2950 1250 50  0001 C CNN
F 3 "" H 2950 1250 50  0001 C CNN
	1    2950 1250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 1350 2950 1250
Connection ~ 2950 1250
$Comp
L power:GND #PWR?
U 1 1 5FBBA8CE
P 2250 2250
AR Path="/5FBBA8CE" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA8CE" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 2250 2000 50  0001 C CNN
F 1 "GND" H 2255 2077 50  0000 C CNN
F 2 "" H 2250 2250 50  0001 C CNN
F 3 "" H 2250 2250 50  0001 C CNN
	1    2250 2250
	1    0    0    -1  
$EndComp
Wire Notes Line
	3950 2550 3950 900 
Wire Notes Line
	3950 900  600  900 
Text Notes 2000 900  0    50   ~ 0
Humidity Sensor #1
$Comp
L Components:0.1u C?
U 1 1 5FBBA8DD
P 2950 1550
AR Path="/5FBBA8DD" Ref="C?"  Part="1" 
AR Path="/5FB89B52/5FBBA8DD" Ref="C1"  Part="1" 
F 0 "C1" V 3154 1678 50  0000 L CNN
F 1 "0.1u" V 3245 1678 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3300 1600 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/GRM022R61A104ME01L.pdf" H 3300 1500 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT" H 3300 1400 50  0001 L CNN "Description"
F 5 "0.22" H 3300 1300 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 3300 1200 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM022R61A104ME01L" H 3300 1100 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM022R61A104ME1L" H 3300 1000 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM022R61A104ME1L" H 3300 900 50  0001 L CNN "Mouser Price/Stock"
	1    2950 1550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA8E3
P 2950 2050
AR Path="/5FBBA8E3" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA8E3" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 2950 1800 50  0001 C CNN
F 1 "GND" H 2955 1877 50  0000 C CNN
F 2 "" H 2950 2050 50  0001 C CNN
F 3 "" H 2950 2050 50  0001 C CNN
	1    2950 2050
	1    0    0    -1  
$EndComp
$Comp
L Components:LPS22HHTR IC?
U 1 1 5FBBA8EF
P 5150 3650
AR Path="/5FBBA8EF" Ref="IC?"  Part="1" 
AR Path="/5FB89B52/5FBBA8EF" Ref="IC3"  Part="1" 
F 0 "IC3" H 6150 3915 50  0000 C CNN
F 1 "LPS22HHTR" H 6150 3824 50  0000 C CNN
F 2 "Components:LPS22HHTR" H 7000 3750 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/lps22hhtr/stmicroelectronics" H 7000 3650 50  0001 L CNN
F 4 "STMICROELECTRONICS - LPS22HHTR - PRESSURE SENSOR, HLGA-10" H 7000 3550 50  0001 L CNN "Description"
F 5 "0.8" H 7000 3450 50  0001 L CNN "Height"
F 6 "STMicroelectronics" H 7000 3350 50  0001 L CNN "Manufacturer_Name"
F 7 "LPS22HHTR" H 7000 3250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "511-LPS22HHTR" H 7000 3150 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=511-LPS22HHTR" H 7000 3050 50  0001 L CNN "Mouser Price/Stock"
	1    5150 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA8F6
P 5150 3850
AR Path="/5FBBA8F6" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA8F6" Ref="#PWR043"  Part="1" 
F 0 "#PWR043" H 5150 3600 50  0001 C CNN
F 1 "GND" V 5155 3722 50  0000 R CNN
F 2 "" H 5150 3850 50  0001 C CNN
F 3 "" H 5150 3850 50  0001 C CNN
	1    5150 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 3950 5150 3950
Wire Wire Line
	4750 3750 5150 3750
Wire Wire Line
	5150 4050 4750 4050
$Comp
L power:GND #PWR?
U 1 1 5FBBA903
P 7150 3750
AR Path="/5FBBA903" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA903" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 7150 3500 50  0001 C CNN
F 1 "GND" V 7155 3622 50  0000 R CNN
F 2 "" H 7150 3750 50  0001 C CNN
F 3 "" H 7150 3750 50  0001 C CNN
	1    7150 3750
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA909
P 7150 3850
AR Path="/5FBBA909" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA909" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 7150 3600 50  0001 C CNN
F 1 "GND" V 7155 3722 50  0000 R CNN
F 2 "" H 7150 3850 50  0001 C CNN
F 3 "" H 7150 3850 50  0001 C CNN
	1    7150 3850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA90F
P 7150 3950
AR Path="/5FBBA90F" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA90F" Ref="#PWR049"  Part="1" 
F 0 "#PWR049" H 7150 3700 50  0001 C CNN
F 1 "GND" V 7155 3822 50  0000 R CNN
F 2 "" H 7150 3950 50  0001 C CNN
F 3 "" H 7150 3950 50  0001 C CNN
	1    7150 3950
	0    -1   -1   0   
$EndComp
Wire Notes Line
	7850 4350 7850 2700
Wire Notes Line
	7850 2700 4100 2700
Wire Notes Line
	4100 2700 4100 4350
Wire Notes Line
	4100 4350 7850 4350
$Comp
L Components:0.1u C?
U 1 1 5FBBA91F
P 5150 3150
AR Path="/5FBBA91F" Ref="C?"  Part="1" 
AR Path="/5FB89B52/5FBBA91F" Ref="C4"  Part="1" 
F 0 "C4" V 5354 3278 50  0000 L CNN
F 1 "0.1u" V 5445 3278 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5500 3200 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/GRM022R61A104ME01L.pdf" H 5500 3100 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT" H 5500 3000 50  0001 L CNN "Description"
F 5 "0.22" H 5500 2900 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 5500 2800 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM022R61A104ME01L" H 5500 2700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM022R61A104ME1L" H 5500 2600 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM022R61A104ME1L" H 5500 2500 50  0001 L CNN "Mouser Price/Stock"
	1    5150 3150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA925
P 5150 3150
AR Path="/5FBBA925" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA925" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 5150 2900 50  0001 C CNN
F 1 "GND" H 5155 2977 50  0000 C CNN
F 2 "" H 5150 3150 50  0001 C CNN
F 3 "" H 5150 3150 50  0001 C CNN
	1    5150 3150
	-1   0    0    1   
$EndComp
Text Notes 5650 2700 0    50   ~ 0
Pressure Sensor #2
Wire Notes Line
	8000 2700 11050 2700
Wire Notes Line
	11050 2700 11050 4450
Wire Notes Line
	11050 4450 8000 4450
Wire Notes Line
	8000 4450 8000 2700
Text Notes 8700 2700 0    50   ~ 0
Temperature Sensor #2 (I2C ADDR 001)
$Comp
L Components:0.1u C?
U 1 1 5FBBA937
P 10200 1450
AR Path="/5FBBA937" Ref="C?"  Part="1" 
AR Path="/5FB89B52/5FBBA937" Ref="C5"  Part="1" 
F 0 "C5" V 10404 1578 50  0000 L CNN
F 1 "0.1u" V 10495 1578 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10550 1500 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/GRM022R61A104ME01L.pdf" H 10550 1400 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT" H 10550 1300 50  0001 L CNN "Description"
F 5 "0.22" H 10550 1200 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 10550 1100 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM022R61A104ME01L" H 10550 1000 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM022R61A104ME1L" H 10550 900 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM022R61A104ME1L" H 10550 800 50  0001 L CNN "Mouser Price/Stock"
	1    10200 1450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA93D
P 10200 1450
AR Path="/5FBBA93D" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA93D" Ref="#PWR057"  Part="1" 
F 0 "#PWR057" H 10200 1200 50  0001 C CNN
F 1 "GND" H 10205 1277 50  0000 C CNN
F 2 "" H 10200 1450 50  0001 C CNN
F 3 "" H 10200 1450 50  0001 C CNN
	1    10200 1450
	-1   0    0    1   
$EndComp
$Comp
L Components:MCP9808-E_MS IC?
U 1 1 5FBBA962
P 9100 1950
AR Path="/5FBBA962" Ref="IC?"  Part="1" 
AR Path="/5FB89B52/5FBBA962" Ref="IC4"  Part="1" 
F 0 "IC4" H 9650 2215 50  0000 C CNN
F 1 "MCP9808-E_MS" H 9650 2124 50  0000 C CNN
F 2 "Components:SOP65P490X110-8N" H 10050 2050 50  0001 L CNN
F 3 "http://docs-emea.rs-online.com/webdocs/1381/0900766b813818c5.pdf" H 10050 1950 50  0001 L CNN
F 4 " +/-0.5C Maximum Accuracy Digital Temperature Sensor" H 10050 1850 50  0001 L CNN "Description"
F 5 "1.1" H 10050 1750 50  0001 L CNN "Height"
F 6 "Microchip" H 10050 1650 50  0001 L CNN "Manufacturer_Name"
F 7 "MCP9808-E/MS" H 10050 1550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "579-MCP9808-E/MS" H 10050 1450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=579-MCP9808-E%2FMS" H 10050 1350 50  0001 L CNN "Mouser Price/Stock"
F 10 "7709892P" H 10050 1250 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/7709892P" H 10050 1150 50  0001 L CNN "RS Price/Stock"
F 12 "70389090" H 10050 1050 50  0001 L CNN "Allied_Number"
F 13 "http://www.alliedelec.com/microchip-technology-inc-mcp9808-e-ms/70389090/" H 10050 950 50  0001 L CNN "Allied Price/Stock"
	1    9100 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA96B
P 10200 2050
AR Path="/5FBBA96B" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA96B" Ref="#PWR052"  Part="1" 
F 0 "#PWR052" H 10200 1800 50  0001 C CNN
F 1 "GND" V 10205 1922 50  0000 R CNN
F 2 "" H 10200 2050 50  0001 C CNN
F 3 "" H 10200 2050 50  0001 C CNN
	1    10200 2050
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA971
P 10200 4050
AR Path="/5FBBA971" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA971" Ref="#PWR055"  Part="1" 
F 0 "#PWR055" H 10200 3800 50  0001 C CNN
F 1 "GND" V 10205 3922 50  0000 R CNN
F 2 "" H 10200 4050 50  0001 C CNN
F 3 "" H 10200 4050 50  0001 C CNN
	1    10200 4050
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA977
P 10200 4150
AR Path="/5FBBA977" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA977" Ref="#PWR056"  Part="1" 
F 0 "#PWR056" H 10200 3900 50  0001 C CNN
F 1 "GND" V 10205 4022 50  0000 R CNN
F 2 "" H 10200 4150 50  0001 C CNN
F 3 "" H 10200 4150 50  0001 C CNN
	1    10200 4150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA97D
P 9100 4150
AR Path="/5FBBA97D" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA97D" Ref="#PWR051"  Part="1" 
F 0 "#PWR051" H 9100 3900 50  0001 C CNN
F 1 "GND" V 9105 4022 50  0000 R CNN
F 2 "" H 9100 4150 50  0001 C CNN
F 3 "" H 9100 4150 50  0001 C CNN
	1    9100 4150
	0    1    1    0   
$EndComp
$Comp
L Components:0.1u C?
U 1 1 5FBBA989
P 10200 3350
AR Path="/5FBBA989" Ref="C?"  Part="1" 
AR Path="/5FB89B52/5FBBA989" Ref="C6"  Part="1" 
F 0 "C6" V 10404 3478 50  0000 L CNN
F 1 "0.1u" V 10495 3478 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10550 3400 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/GRM022R61A104ME01L.pdf" H 10550 3300 50  0001 L CNN
F 4 "Multilayer Ceramic Capacitors MLCC - SMD/SMT" H 10550 3200 50  0001 L CNN "Description"
F 5 "0.22" H 10550 3100 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 10550 3000 50  0001 L CNN "Manufacturer_Name"
F 7 "GRM022R61A104ME01L" H 10550 2900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "81-GRM022R61A104ME1L" H 10550 2800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=81-GRM022R61A104ME1L" H 10550 2700 50  0001 L CNN "Mouser Price/Stock"
	1    10200 3350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FBBA98F
P 10200 3350
AR Path="/5FBBA98F" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FBBA98F" Ref="#PWR058"  Part="1" 
F 0 "#PWR058" H 10200 3100 50  0001 C CNN
F 1 "GND" H 10205 3177 50  0000 C CNN
F 2 "" H 10200 3350 50  0001 C CNN
F 3 "" H 10200 3350 50  0001 C CNN
	1    10200 3350
	-1   0    0    1   
$EndComp
$Comp
L Components:MCP9808-E_MS IC?
U 1 1 5FBBA9B8
P 9100 3850
AR Path="/5FBBA9B8" Ref="IC?"  Part="1" 
AR Path="/5FB89B52/5FBBA9B8" Ref="IC5"  Part="1" 
F 0 "IC5" H 9650 4115 50  0000 C CNN
F 1 "MCP9808-E_MS" H 9650 4024 50  0000 C CNN
F 2 "Components:SOP65P490X110-8N" H 10050 3950 50  0001 L CNN
F 3 "http://docs-emea.rs-online.com/webdocs/1381/0900766b813818c5.pdf" H 10050 3850 50  0001 L CNN
F 4 " +/-0.5C Maximum Accuracy Digital Temperature Sensor" H 10050 3750 50  0001 L CNN "Description"
F 5 "1.1" H 10050 3650 50  0001 L CNN "Height"
F 6 "Microchip" H 10050 3550 50  0001 L CNN "Manufacturer_Name"
F 7 "MCP9808-E/MS" H 10050 3450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "579-MCP9808-E/MS" H 10050 3350 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=579-MCP9808-E%2FMS" H 10050 3250 50  0001 L CNN "Mouser Price/Stock"
F 10 "7709892P" H 10050 3150 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/7709892P" H 10050 3050 50  0001 L CNN "RS Price/Stock"
F 12 "70389090" H 10050 2950 50  0001 L CNN "Allied_Number"
F 13 "http://www.alliedelec.com/microchip-technology-inc-mcp9808-e-ms/70389090/" H 10050 2850 50  0001 L CNN "Allied Price/Stock"
	1    9100 3850
	1    0    0    -1  
$EndComp
NoConn ~ 9100 4050
NoConn ~ 9100 2150
Wire Notes Line
	600  900  600  2550
Wire Notes Line
	600  2550 3950 2550
Wire Wire Line
	7300 1850 7150 1850
Wire Wire Line
	7300 3650 7150 3650
Wire Wire Line
	1550 3150 1300 3150
Wire Wire Line
	7150 4050 7300 4050
Wire Wire Line
	7150 2250 7250 2250
Wire Wire Line
	2950 1550 3200 1550
Wire Wire Line
	2950 3350 3200 3350
Text HLabel 1300 1250 0    50   BiDi ~ 0
I2C_SDA_HUM_1
Text HLabel 1300 1550 0    50   Input ~ 0
I2C_SCL_HUM_1
Text HLabel 3350 1450 2    50   Input ~ 0
HUMIDITY_RST1
Text HLabel 1300 3050 0    50   BiDi ~ 0
I2C_SDA_HUM_2
Text HLabel 1300 3350 0    50   Input ~ 0
I2C_SCL_HUM_2
Text HLabel 3350 3250 2    50   Input ~ 0
HUMIDITY_RST2
Text HLabel 8750 1950 0    50   BiDi ~ 0
I2C_SDA_TMP_1
Text HLabel 8750 2050 0    50   Input ~ 0
I2C_SCL_TMP_1
Text HLabel 8750 3850 0    50   BiDi ~ 0
I2C_SDA_TMP_2
Text HLabel 8750 3950 0    50   Input ~ 0
I2C_SCL_TMP_2
Text HLabel 7300 1850 2    50   Input ~ 0
SPI_NSS_1
Text HLabel 7300 3650 2    50   Input ~ 0
SPI_NSS_2
Text HLabel 4750 1950 0    50   Input ~ 0
SPI_SCK_1
Text HLabel 4750 2150 0    50   Input ~ 0
SPI_MOSI_1
Text HLabel 4750 2250 0    50   Output ~ 0
SPI_MISO_1
Text HLabel 4750 3750 0    50   Input ~ 0
SPI_SCK_2
Text HLabel 4750 3950 0    50   Input ~ 0
SPI_MOSI_2
Text HLabel 4750 4050 0    50   Output ~ 0
SPI_MISO_2
Text GLabel 3200 1550 2    50   Input ~ 0
3.3V
Text GLabel 5000 1850 0    50   Input ~ 0
3.3V
Text GLabel 1300 3150 0    50   Input ~ 0
3.3V
Text GLabel 3200 3350 2    50   Input ~ 0
3.3V
Text GLabel 5000 3650 0    50   Input ~ 0
3.3V
Text GLabel 7300 4050 2    50   Input ~ 0
3.3V
Text GLabel 7250 2250 2    50   Input ~ 0
3.3V
Text GLabel 10350 1950 2    50   Input ~ 0
3.3V
Text GLabel 10350 3850 2    50   Input ~ 0
3.3V
Text GLabel 10200 3950 2    50   Input ~ 0
3.3V
$Comp
L Components:FGD-04D IC?
U 1 1 5F6D8A48
P 1400 5150
AR Path="/5F6D8A48" Ref="IC?"  Part="1" 
AR Path="/5FB89B52/5F6D8A48" Ref="IC1"  Part="1" 
F 0 "IC1" H 1400 5565 50  0000 C CNN
F 1 "FGD-04D" H 1400 5474 50  0000 C CNN
F 2 "Package_DFN_QFN:DFN-6-1EP_2x2mm_P0.5mm_EP0.61x1.42mm" H 1400 5150 50  0001 C CNN
F 3 "" H 1400 5150 50  0001 C CNN
	1    1400 5150
	1    0    0    -1  
$EndComp
Text GLabel 850  5000 0    50   Input ~ 0
5V
$Comp
L power:GND #PWR?
U 1 1 5F6DA989
P 950 5350
AR Path="/5F6DA989" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5F6DA989" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 950 5100 50  0001 C CNN
F 1 "GND" H 955 5177 50  0000 C CNN
F 2 "" H 950 5350 50  0001 C CNN
F 3 "" H 950 5350 50  0001 C CNN
	1    950  5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 5300 950  5300
Wire Wire Line
	950  5300 950  5350
Text HLabel 1800 5000 2    50   Input ~ 0
I2C_SCL_FG
Text HLabel 1800 5150 2    50   BiDi ~ 0
I2C_SDA_FG
Text HLabel 2100 5300 2    50   Output ~ 0
FG_VDOSE
Text Notes 1450 4500 0    50   ~ 0
Floating Gate Dosimeter
Wire Notes Line
	3200 4500 3200 5800
Wire Notes Line
	3200 5800 600  5800
Wire Notes Line
	600  4500 600  5800
$Comp
L Device:R R?
U 1 1 60839EAB
P 1350 2100
AR Path="/60839EAB" Ref="R?"  Part="1" 
AR Path="/5FB89B52/60839EAB" Ref="R27"  Part="1" 
F 0 "R27" H 1150 2150 50  0000 L CNN
F 1 "1.5k" H 1100 2050 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1280 2100 50  0001 C CNN
F 3 "~" H 1350 2100 50  0001 C CNN
	1    1350 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6083A1C7
P 1550 2100
AR Path="/6083A1C7" Ref="R?"  Part="1" 
AR Path="/5FB89B52/6083A1C7" Ref="R29"  Part="1" 
F 0 "R29" H 1600 1950 50  0000 L CNN
F 1 "1.5k" H 1620 2055 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1480 2100 50  0001 C CNN
F 3 "~" H 1550 2100 50  0001 C CNN
	1    1550 2100
	1    0    0    -1  
$EndComp
Text HLabel 1300 2350 0    50   BiDi ~ 0
I2C_SDA_HUM_1
Text HLabel 1300 2450 0    50   Input ~ 0
I2C_SCL_HUM_1
Text GLabel 1450 1850 1    50   Input ~ 0
3.3V
Wire Wire Line
	1350 1950 1350 1900
Wire Wire Line
	1350 1900 1450 1900
Wire Wire Line
	1550 1900 1550 1950
Wire Wire Line
	1450 1850 1450 1900
Connection ~ 1450 1900
Wire Wire Line
	1450 1900 1550 1900
Wire Wire Line
	1300 2350 1350 2350
Wire Wire Line
	1350 2350 1350 2250
Wire Wire Line
	1300 2450 1550 2450
Wire Wire Line
	1550 2450 1550 2250
$Comp
L Device:R R?
U 1 1 6083FEAF
P 1300 3900
AR Path="/6083FEAF" Ref="R?"  Part="1" 
AR Path="/5FB89B52/6083FEAF" Ref="R26"  Part="1" 
F 0 "R26" H 1100 3950 50  0000 L CNN
F 1 "1.5k" H 1050 3850 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1230 3900 50  0001 C CNN
F 3 "~" H 1300 3900 50  0001 C CNN
	1    1300 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 608401B1
P 1500 3900
AR Path="/608401B1" Ref="R?"  Part="1" 
AR Path="/5FB89B52/608401B1" Ref="R28"  Part="1" 
F 0 "R28" H 1570 3946 50  0000 L CNN
F 1 "1.5k" H 1570 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1430 3900 50  0001 C CNN
F 3 "~" H 1500 3900 50  0001 C CNN
	1    1500 3900
	1    0    0    -1  
$EndComp
Text GLabel 1400 3650 1    50   Input ~ 0
3.3V
Wire Wire Line
	1300 3750 1300 3700
Wire Wire Line
	1300 3700 1400 3700
Wire Wire Line
	1500 3700 1500 3750
Wire Wire Line
	1400 3650 1400 3700
Connection ~ 1400 3700
Wire Wire Line
	1400 3700 1500 3700
Text HLabel 1300 4150 0    50   BiDi ~ 0
I2C_SDA_HUM_2
Text HLabel 1300 4250 0    50   Input ~ 0
I2C_SCL_HUM_2
Wire Wire Line
	1300 4150 1300 4050
Wire Wire Line
	1300 4250 1500 4250
Wire Wire Line
	1500 4250 1500 4050
Wire Notes Line
	600  4500 3200 4500
$Comp
L Device:R R?
U 1 1 62317618
P 3300 3100
AR Path="/62317618" Ref="R?"  Part="1" 
AR Path="/5FB89B52/62317618" Ref="R100"  Part="1" 
F 0 "R100" H 3370 3146 50  0000 L CNN
F 1 "10k" H 3370 3055 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3230 3100 50  0001 C CNN
F 3 "~" H 3300 3100 50  0001 C CNN
	1    3300 3100
	1    0    0    -1  
$EndComp
Connection ~ 3300 3250
Wire Wire Line
	3300 3250 2950 3250
Text GLabel 3300 2950 1    50   Input ~ 0
3.3V
Text HLabel 1000 5150 0    50   Input ~ 0
FG_VB
$Comp
L Device:R R?
U 1 1 6235CD14
P 1950 5300
AR Path="/6235CD14" Ref="R?"  Part="1" 
AR Path="/5FB89B52/6235CD14" Ref="R84"  Part="1" 
F 0 "R84" V 1850 5350 50  0000 L CNN
F 1 "0ohms" V 1850 5100 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1880 5300 50  0001 C CNN
F 3 "~" H 1950 5300 50  0001 C CNN
	1    1950 5300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10350 1950 10200 1950
Connection ~ 10200 1950
Wire Wire Line
	10350 3850 10200 3850
Connection ~ 10200 3850
Wire Wire Line
	5150 3650 5000 3650
Connection ~ 5150 3650
Wire Wire Line
	5150 1850 5000 1850
Connection ~ 5150 1850
Connection ~ 2950 3350
Connection ~ 2950 1550
$Comp
L Device:C C?
U 1 1 6232F033
P 900 4850
AR Path="/6232F033" Ref="C?"  Part="1" 
AR Path="/5FBFE149/6232F033" Ref="C?"  Part="1" 
AR Path="/5FB89B52/6232F033" Ref="C44"  Part="1" 
F 0 "C44" H 1015 4896 50  0000 L CNN
F 1 "1n" H 1015 4805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 938 4700 50  0001 C CNN
F 3 "~" H 900 4850 50  0001 C CNN
	1    900  4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 5000 900  5000
Connection ~ 900  5000
Wire Wire Line
	900  5000 850  5000
$Comp
L power:GND #PWR?
U 1 1 6233708F
P 900 4700
AR Path="/6233708F" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/6233708F" Ref="#PWR0149"  Part="1" 
F 0 "#PWR0149" H 900 4450 50  0001 C CNN
F 1 "GND" H 905 4527 50  0000 C CNN
F 2 "" H 900 4700 50  0001 C CNN
F 3 "" H 900 4700 50  0001 C CNN
	1    900  4700
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 624047B8
P 3300 1300
AR Path="/624047B8" Ref="R?"  Part="1" 
AR Path="/5FB89B52/624047B8" Ref="R99"  Part="1" 
F 0 "R99" H 3370 1346 50  0000 L CNN
F 1 "10k" H 3370 1255 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3230 1300 50  0001 C CNN
F 3 "~" H 3300 1300 50  0001 C CNN
	1    3300 1300
	1    0    0    -1  
$EndComp
Text GLabel 3300 1150 1    50   Input ~ 0
3.3V
Connection ~ 3300 1450
Wire Wire Line
	3300 1450 2950 1450
Wire Wire Line
	8750 3850 9100 3850
Wire Wire Line
	8750 3950 9100 3950
Wire Wire Line
	8750 1950 9100 1950
Wire Wire Line
	8750 2050 9100 2050
$EndSCHEMATC
