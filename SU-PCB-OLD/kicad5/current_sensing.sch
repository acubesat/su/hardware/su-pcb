EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Components:INA226AQDGSRQ1 IC?
U 1 1 5F741B18
P 5750 2650
AR Path="/5FFAB9B8/5F741B18" Ref="IC?"  Part="1" 
AR Path="/5F73FF05/5F741B18" Ref="IC12"  Part="1" 
F 0 "IC12" H 6350 2915 50  0000 C CNN
F 1 "INA228AQDGSRQ1" H 6350 2824 50  0000 C CNN
F 2 "Components:SOP50P490X110-10N" H 6800 2750 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/ina226-q1" H 6800 2650 50  0001 L CNN
F 4 "AEC-Q100, 36V, Bi-Directional, High Accuracy, Low-/High-Side, I2C Out Current/Power Monitor w/Alert" H 6800 2550 50  0001 L CNN "Description"
F 5 "1.1" H 6800 2450 50  0001 L CNN "Height"
F 6 "595-INA226AQDGSRQ1" H 6800 2350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/INA226AQDGSRQ1?qs=uNh9J9Y3wTf1B1%2FBFFp7SA%3D%3D" H 6800 2250 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 6800 2150 50  0001 L CNN "Manufacturer_Name"
F 9 "INA226AQDGSRQ1" H 6800 2050 50  0001 L CNN "Manufacturer_Part_Number"
	1    5750 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2650 5700 2650
Wire Wire Line
	5700 2650 5700 2700
Wire Wire Line
	5700 2750 5750 2750
Connection ~ 5700 2700
Wire Wire Line
	5700 2700 5700 2750
$Comp
L power:GND #PWR?
U 1 1 5F741B23
P 5650 2700
AR Path="/5F741B23" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/5F741B23" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/5F741B23" Ref="#PWR?"  Part="1" 
AR Path="/5F73FF05/5F741B23" Ref="#PWR0141"  Part="1" 
F 0 "#PWR0141" H 5650 2450 50  0001 C CNN
F 1 "GND" H 5655 2527 50  0000 C CNN
F 2 "" H 5650 2700 50  0001 C CNN
F 3 "" H 5650 2700 50  0001 C CNN
	1    5650 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	5700 2700 5650 2700
$Comp
L Device:R R?
U 1 1 5F741B36
P 4700 2550
AR Path="/5FFAB9B8/5F741B36" Ref="R?"  Part="1" 
AR Path="/5F73FF05/5F741B36" Ref="R91"  Part="1" 
F 0 "R91" H 4770 2596 50  0000 L CNN
F 1 "10k" H 4770 2505 50  0000 L CNN
F 2 "Inductor_SMD:L_0402_1005Metric" V 4630 2550 50  0001 C CNN
F 3 "~" H 4700 2550 50  0001 C CNN
	1    4700 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 2700 4700 2850
Wire Wire Line
	4700 2850 5750 2850
Wire Wire Line
	5750 2950 5000 2950
Wire Wire Line
	5750 3050 5300 3050
Text Label 4700 2850 0    50   ~ 0
FLOW_PUMP_ALERT
Text Label 5000 2950 0    50   ~ 0
I2C_SDA_FLOW
Text Label 5300 3050 0    50   ~ 0
I2C_SCL_FLOW
$Comp
L power:GND #PWR?
U 1 1 5F741B4B
P 7200 2950
AR Path="/5F741B4B" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/5F741B4B" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/5F741B4B" Ref="#PWR?"  Part="1" 
AR Path="/5F73FF05/5F741B4B" Ref="#PWR0145"  Part="1" 
F 0 "#PWR0145" H 7200 2700 50  0001 C CNN
F 1 "GND" H 7205 2777 50  0000 C CNN
F 2 "" H 7200 2950 50  0001 C CNN
F 3 "" H 7200 2950 50  0001 C CNN
	1    7200 2950
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F741B51
P 7000 3250
AR Path="/5FFAB9B8/5F741B51" Ref="C?"  Part="1" 
AR Path="/5F73FF05/5F741B51" Ref="C40"  Part="1" 
F 0 "C40" H 7115 3296 50  0000 L CNN
F 1 "0.1u" H 7115 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7038 3100 50  0001 C CNN
F 3 "~" H 7000 3250 50  0001 C CNN
	1    7000 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F741B57
P 7000 3450
AR Path="/5F741B57" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/5F741B57" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/5F741B57" Ref="#PWR?"  Part="1" 
AR Path="/5F73FF05/5F741B57" Ref="#PWR0143"  Part="1" 
F 0 "#PWR0143" H 7000 3200 50  0001 C CNN
F 1 "GND" H 7005 3277 50  0000 C CNN
F 2 "" H 7000 3450 50  0001 C CNN
F 3 "" H 7000 3450 50  0001 C CNN
	1    7000 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3450 7000 3400
Text GLabel 7000 3050 2    50   Input ~ 0
3.3V
Wire Wire Line
	6950 3050 7000 3050
Wire Wire Line
	7000 3050 7000 3100
Wire Wire Line
	6950 2950 7200 2950
Wire Wire Line
	6950 2850 7000 2850
Wire Wire Line
	7000 2850 7000 2800
Wire Wire Line
	7000 2750 6950 2750
Wire Wire Line
	6950 2650 7300 2650
Wire Wire Line
	7000 2800 7300 2800
Connection ~ 7000 2800
Wire Wire Line
	7000 2800 7000 2750
Wire Wire Line
	7300 2650 7300 2550
Wire Wire Line
	7300 2550 7450 2550
Wire Wire Line
	7300 2800 7300 2850
Wire Wire Line
	7300 2850 7450 2850
$Comp
L Device:R R?
U 1 1 5F741B6D
P 7450 2700
AR Path="/5FFAB9B8/5F741B6D" Ref="R?"  Part="1" 
AR Path="/5F73FF05/5F741B6D" Ref="R97"  Part="1" 
F 0 "R97" H 7520 2746 50  0000 L CNN
F 1 "50m" H 7520 2655 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 7380 2700 50  0001 C CNN
F 3 "~" H 7450 2700 50  0001 C CNN
	1    7450 2700
	1    0    0    -1  
$EndComp
Text GLabel 4700 2300 1    50   Input ~ 0
3.3V
Text GLabel 7450 2500 1    50   Input ~ 0
5V
Wire Wire Line
	7450 2500 7450 2550
Connection ~ 7450 2550
Wire Wire Line
	7450 2850 7550 2850
Connection ~ 7450 2850
Text HLabel 4700 2850 0    50   Output ~ 0
FLOW_PUMP_ALERT
Text HLabel 5000 2950 0    50   BiDi ~ 0
I2C_SDA_FLOW_PUMP
Text HLabel 5300 3050 0    50   Input ~ 0
I2C_SCL_FLOW_PUMP
$Comp
L Components:INA226AQDGSRQ1 IC?
U 1 1 5F743DEC
P 5750 4300
AR Path="/5FFAB9B8/5F743DEC" Ref="IC?"  Part="1" 
AR Path="/6009AF74/5F743DEC" Ref="IC?"  Part="1" 
AR Path="/5F73FF05/5F743DEC" Ref="IC13"  Part="1" 
F 0 "IC13" H 6350 4565 50  0000 C CNN
F 1 "INA228AQDGSRQ1" H 6350 4474 50  0000 C CNN
F 2 "Components:SOP50P490X110-10N" H 6800 4400 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/ina226-q1" H 6800 4300 50  0001 L CNN
F 4 "AEC-Q100, 36V, Bi-Directional, High Accuracy, Low-/High-Side, I2C Out Current/Power Monitor w/Alert" H 6800 4200 50  0001 L CNN "Description"
F 5 "1.1" H 6800 4100 50  0001 L CNN "Height"
F 6 "595-INA226AQDGSRQ1" H 6800 4000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/INA226AQDGSRQ1?qs=uNh9J9Y3wTf1B1%2FBFFp7SA%3D%3D" H 6800 3900 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 6800 3800 50  0001 L CNN "Manufacturer_Name"
F 9 "INA226AQDGSRQ1" H 6800 3700 50  0001 L CNN "Manufacturer_Part_Number"
	1    5750 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F743DF7
P 5650 4300
AR Path="/5F743DF7" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/5F743DF7" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/5F743DF7" Ref="#PWR?"  Part="1" 
AR Path="/5F73FF05/5F743DF7" Ref="#PWR0142"  Part="1" 
F 0 "#PWR0142" H 5650 4050 50  0001 C CNN
F 1 "GND" H 5655 4127 50  0000 C CNN
F 2 "" H 5650 4300 50  0001 C CNN
F 3 "" H 5650 4300 50  0001 C CNN
	1    5650 4300
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F743E0A
P 4700 4200
AR Path="/5FFAB9B8/5F743E0A" Ref="R?"  Part="1" 
AR Path="/6009AF74/5F743E0A" Ref="R?"  Part="1" 
AR Path="/5F73FF05/5F743E0A" Ref="R92"  Part="1" 
F 0 "R92" H 4770 4246 50  0000 L CNN
F 1 "10k" H 4770 4155 50  0000 L CNN
F 2 "Inductor_SMD:L_0402_1005Metric" V 4630 4200 50  0001 C CNN
F 3 "~" H 4700 4200 50  0001 C CNN
	1    4700 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4350 4700 4500
Wire Wire Line
	4700 4500 5750 4500
Wire Wire Line
	5750 4600 5000 4600
Wire Wire Line
	5750 4700 5300 4700
Text Label 4700 4500 0    50   ~ 0
CONTROL_PUMP_ALERT
Text Label 5000 4600 0    50   ~ 0
I2C_SDA_CONTROL
Text Label 5300 4700 0    50   ~ 0
I2C_SCL_CONTROL
$Comp
L power:GND #PWR?
U 1 1 5F743E1F
P 7200 4600
AR Path="/5F743E1F" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/5F743E1F" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/5F743E1F" Ref="#PWR?"  Part="1" 
AR Path="/5F73FF05/5F743E1F" Ref="#PWR0146"  Part="1" 
F 0 "#PWR0146" H 7200 4350 50  0001 C CNN
F 1 "GND" H 7205 4427 50  0000 C CNN
F 2 "" H 7200 4600 50  0001 C CNN
F 3 "" H 7200 4600 50  0001 C CNN
	1    7200 4600
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F743E25
P 7000 4900
AR Path="/5FFAB9B8/5F743E25" Ref="C?"  Part="1" 
AR Path="/6009AF74/5F743E25" Ref="C?"  Part="1" 
AR Path="/5F73FF05/5F743E25" Ref="C41"  Part="1" 
F 0 "C41" H 7115 4946 50  0000 L CNN
F 1 "0.1u" H 7115 4855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7038 4750 50  0001 C CNN
F 3 "~" H 7000 4900 50  0001 C CNN
	1    7000 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F743E2B
P 7000 5100
AR Path="/5F743E2B" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/5F743E2B" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/5F743E2B" Ref="#PWR?"  Part="1" 
AR Path="/5F73FF05/5F743E2B" Ref="#PWR0144"  Part="1" 
F 0 "#PWR0144" H 7000 4850 50  0001 C CNN
F 1 "GND" H 7005 4927 50  0000 C CNN
F 2 "" H 7000 5100 50  0001 C CNN
F 3 "" H 7000 5100 50  0001 C CNN
	1    7000 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 5100 7000 5050
Text GLabel 7000 4700 2    50   Input ~ 0
3.3V
Wire Wire Line
	6950 4700 7000 4700
Wire Wire Line
	7000 4700 7000 4750
Wire Wire Line
	6950 4600 7200 4600
Wire Wire Line
	6950 4500 7000 4500
Wire Wire Line
	7000 4500 7000 4450
Wire Wire Line
	7000 4400 6950 4400
Wire Wire Line
	6950 4300 7300 4300
Wire Wire Line
	7000 4450 7300 4450
Connection ~ 7000 4450
Wire Wire Line
	7000 4450 7000 4400
Wire Wire Line
	7300 4300 7300 4200
Wire Wire Line
	7300 4200 7450 4200
Wire Wire Line
	7300 4450 7300 4500
Wire Wire Line
	7300 4500 7450 4500
$Comp
L Device:R R?
U 1 1 5F743E41
P 7450 4350
AR Path="/5FFAB9B8/5F743E41" Ref="R?"  Part="1" 
AR Path="/6009AF74/5F743E41" Ref="R?"  Part="1" 
AR Path="/5F73FF05/5F743E41" Ref="R98"  Part="1" 
F 0 "R98" H 7520 4396 50  0000 L CNN
F 1 "50m" H 7520 4305 50  0000 L CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 7380 4350 50  0001 C CNN
F 3 "~" H 7450 4350 50  0001 C CNN
	1    7450 4350
	1    0    0    -1  
$EndComp
Text GLabel 4700 3950 1    50   Input ~ 0
3.3V
Text GLabel 7450 4150 1    50   Input ~ 0
5V
Wire Wire Line
	7450 4150 7450 4200
Connection ~ 7450 4200
Wire Wire Line
	7450 4500 7550 4500
Connection ~ 7450 4500
Text HLabel 4700 4500 0    50   Output ~ 0
CONTROL_PUMP_ALERT
Text HLabel 5000 4600 0    50   BiDi ~ 0
I2C_SDA_CONTROL_PUMP
Text HLabel 5300 4700 0    50   Input ~ 0
I2C_SCL_CONTROL_PUMP
Text HLabel 7550 2850 2    50   Output ~ 0
FLOW_VCC
Text HLabel 7550 4500 2    50   Output ~ 0
CONTROL_VCC
Wire Wire Line
	4700 3950 4700 4050
Wire Wire Line
	4700 2300 4700 2400
Wire Wire Line
	5650 4300 5750 4300
Text GLabel 5750 4400 0    50   Input ~ 0
3.3V
$EndSCHEMATC
