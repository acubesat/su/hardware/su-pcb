EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5F9D23D4
P 4600 2250
F 0 "R1" H 4670 2296 50  0000 L CNN
F 1 "10k" H 4670 2205 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4530 2250 50  0001 C CNN
F 3 "~" H 4600 2250 50  0001 C CNN
	1    4600 2250
	-1   0    0    1   
$EndComp
Text HLabel 4950 3000 2    50   Input ~ 0
CONTROL_VALVE_POSITIVE_1
Text HLabel 4150 2600 0    50   Input ~ 0
CONTROL_VALVE_1_SIGNAL
Text HLabel 4150 3750 0    50   Input ~ 0
CONTROL_VALVE_2_SIGNAL
Text HLabel 4150 4900 0    50   Input ~ 0
CONTROL_VALVE_3_SIGNAL
Text HLabel 6450 2600 0    50   Input ~ 0
CONTROL_VALVE_4_SIGNAL
Text HLabel 6450 3750 0    50   Input ~ 0
CONTROL_VALVE_5_SIGNAL
Text HLabel 6450 4900 0    50   Input ~ 0
CONTROL_VALVE_6_SIGNAL
$Comp
L Components:NTK3134NT1G Q1
U 1 1 5F6764D9
P 4650 2600
F 0 "Q1" H 5080 2746 50  0000 L CNN
F 1 "FDV304P" H 5080 2655 50  0000 L CNN
F 2 "Components:FDV304P" H 5100 2550 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 5100 2450 50  0001 L CNN
F 4 "Power MOSFET" H 5100 2350 50  0001 L CNN "Description"
F 5 "0.55" H 5100 2250 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 5100 2150 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 5100 2050 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 5100 1950 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 5100 1850 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 2100 4600 2000
$Comp
L Device:R R2
U 1 1 5F683748
P 4600 3400
F 0 "R2" H 4670 3446 50  0000 L CNN
F 1 "10k" H 4670 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4530 3400 50  0001 C CNN
F 3 "~" H 4600 3400 50  0001 C CNN
	1    4600 3400
	-1   0    0    1   
$EndComp
$Comp
L Components:NTK3134NT1G Q2
U 1 1 5F683755
P 4650 3750
F 0 "Q2" H 5080 3896 50  0000 L CNN
F 1 "FDV304P" H 5080 3805 50  0000 L CNN
F 2 "Components:FDV304P" H 5100 3700 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 5100 3600 50  0001 L CNN
F 4 "Power MOSFET" H 5100 3500 50  0001 L CNN "Description"
F 5 "0.55" H 5100 3400 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 5100 3300 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 5100 3200 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 5100 3100 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 5100 3000 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3250 4600 3200
$Comp
L Device:R R3
U 1 1 5F6854AC
P 4600 4550
F 0 "R3" H 4670 4596 50  0000 L CNN
F 1 "10k" H 4670 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4530 4550 50  0001 C CNN
F 3 "~" H 4600 4550 50  0001 C CNN
	1    4600 4550
	-1   0    0    1   
$EndComp
$Comp
L Components:NTK3134NT1G Q3
U 1 1 5F6854B9
P 4650 4900
F 0 "Q3" H 5080 5046 50  0000 L CNN
F 1 "FDV304P" H 5080 4955 50  0000 L CNN
F 2 "Components:FDV304P" H 5100 4850 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 5100 4750 50  0001 L CNN
F 4 "Power MOSFET" H 5100 4650 50  0001 L CNN "Description"
F 5 "0.55" H 5100 4550 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 5100 4450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 5100 4350 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 5100 4250 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 5100 4150 50  0001 L CNN "Manufacturer_Part_Number"
	1    4650 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4400 4600 4300
$Comp
L Device:R R4
U 1 1 5F69FDC6
P 6850 2250
F 0 "R4" H 6920 2296 50  0000 L CNN
F 1 "10k" H 6920 2205 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6780 2250 50  0001 C CNN
F 3 "~" H 6850 2250 50  0001 C CNN
	1    6850 2250
	-1   0    0    1   
$EndComp
$Comp
L Components:NTK3134NT1G Q4
U 1 1 5F69FDD3
P 6900 2600
F 0 "Q4" H 7330 2746 50  0000 L CNN
F 1 "FDV304P" H 7330 2655 50  0000 L CNN
F 2 "Components:FDV304P" H 7350 2550 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 7350 2450 50  0001 L CNN
F 4 "Power MOSFET" H 7350 2350 50  0001 L CNN "Description"
F 5 "0.55" H 7350 2250 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 7350 2150 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 7350 2050 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 7350 1950 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 7350 1850 50  0001 L CNN "Manufacturer_Part_Number"
	1    6900 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2600 6850 2600
Connection ~ 6850 2600
Wire Wire Line
	6850 2600 6900 2600
Wire Wire Line
	6850 2100 6850 2000
$Comp
L Device:R R5
U 1 1 5F69FDEC
P 6850 3400
F 0 "R5" H 6920 3446 50  0000 L CNN
F 1 "10k" H 6920 3355 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6780 3400 50  0001 C CNN
F 3 "~" H 6850 3400 50  0001 C CNN
	1    6850 3400
	-1   0    0    1   
$EndComp
$Comp
L Components:NTK3134NT1G Q5
U 1 1 5F69FDF9
P 6900 3750
F 0 "Q5" H 7330 3896 50  0000 L CNN
F 1 "FDV304P" H 7330 3805 50  0000 L CNN
F 2 "Components:FDV304P" H 7350 3700 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 7350 3600 50  0001 L CNN
F 4 "Power MOSFET" H 7350 3500 50  0001 L CNN "Description"
F 5 "0.55" H 7350 3400 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 7350 3300 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 7350 3200 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 7350 3100 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 7350 3000 50  0001 L CNN "Manufacturer_Part_Number"
	1    6900 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 3250 6850 3200
$Comp
L Device:R R6
U 1 1 5F69FE12
P 6850 4550
F 0 "R6" H 6920 4596 50  0000 L CNN
F 1 "10k" H 6920 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6780 4550 50  0001 C CNN
F 3 "~" H 6850 4550 50  0001 C CNN
	1    6850 4550
	-1   0    0    1   
$EndComp
$Comp
L Components:NTK3134NT1G Q6
U 1 1 5F69FE1F
P 6900 4900
F 0 "Q6" H 7330 5046 50  0000 L CNN
F 1 "FDV304P" H 7330 4955 50  0000 L CNN
F 2 "Components:FDV304P" H 7350 4850 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 7350 4750 50  0001 L CNN
F 4 "Power MOSFET" H 7350 4650 50  0001 L CNN "Description"
F 5 "0.55" H 7350 4550 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 7350 4450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 7350 4350 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 7350 4250 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 7350 4150 50  0001 L CNN "Manufacturer_Part_Number"
	1    6900 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 4400 6850 4300
Text Notes 5650 1400 0    50   ~ 0
allazo se tlc outputs kai pull-ups
Text Notes 5400 1550 0    50   ~ 0
perimeno kukloma gia na kano ENA implementation
Text GLabel 4600 1850 1    50   Input ~ 0
5V
Text GLabel 6850 1800 1    50   Input ~ 0
5V
Text GLabel 4600 3100 1    50   Input ~ 0
5V
Text GLabel 6850 3100 1    50   Input ~ 0
5V
Text GLabel 4600 4300 1    50   Input ~ 0
5V
Text GLabel 6850 4300 1    50   Input ~ 0
5V
Wire Wire Line
	4450 2600 4600 2600
Wire Wire Line
	4600 2400 4600 2600
Connection ~ 4600 2600
Wire Wire Line
	4600 2600 4650 2600
Wire Wire Line
	4950 2200 4950 2000
Wire Wire Line
	4950 2000 4600 2000
Connection ~ 4600 2000
Wire Wire Line
	4600 2000 4600 1850
Wire Wire Line
	4950 2800 4950 3000
Text HLabel 7200 3000 2    50   Input ~ 0
CONTROL_VALVE_POSITIVE_4
Text HLabel 4950 4200 2    50   Input ~ 0
CONTROL_VALVE_POSITIVE_2
Text HLabel 7200 4200 2    50   Input ~ 0
CONTROL_VALVE_POSITIVE_5
Text HLabel 4950 5350 2    50   Input ~ 0
CONTROL_VALVE_POSITIVE_3
Text HLabel 7200 5350 2    50   Input ~ 0
CONTROL_VALVE_POSITIVE_6
Wire Wire Line
	6850 2400 6850 2600
Wire Wire Line
	7200 2200 7200 2000
Wire Wire Line
	7200 2000 6850 2000
Connection ~ 6850 2000
Wire Wire Line
	6850 2000 6850 1800
Wire Wire Line
	7200 2800 7200 3000
Wire Wire Line
	6750 3750 6850 3750
Wire Wire Line
	6850 3550 6850 3750
Connection ~ 6850 3750
Wire Wire Line
	6850 3750 6900 3750
Wire Wire Line
	7200 3350 7200 3200
Wire Wire Line
	7200 3200 6850 3200
Connection ~ 6850 3200
Wire Wire Line
	6850 3200 6850 3100
Wire Wire Line
	7200 3950 7200 4200
Wire Wire Line
	4950 3950 4950 4200
Wire Wire Line
	4600 3550 4600 3750
Connection ~ 4600 3750
Wire Wire Line
	4600 3750 4650 3750
Wire Wire Line
	4950 3350 4950 3200
Wire Wire Line
	4950 3200 4600 3200
Connection ~ 4600 3200
Wire Wire Line
	4600 3200 4600 3100
Wire Wire Line
	4950 4500 4950 4400
Wire Wire Line
	4950 4400 4600 4400
Connection ~ 4600 4400
Wire Wire Line
	4950 5100 4950 5350
Wire Wire Line
	6750 4900 6850 4900
Wire Wire Line
	7200 5100 7200 5350
Wire Wire Line
	7200 4500 7200 4400
Wire Wire Line
	7200 4400 6850 4400
Connection ~ 6850 4400
Wire Wire Line
	6850 4700 6850 4900
Connection ~ 6850 4900
Wire Wire Line
	6850 4900 6900 4900
Wire Wire Line
	4600 4700 4600 4900
Connection ~ 4600 4900
Wire Wire Line
	4600 4900 4650 4900
$Comp
L Device:R R?
U 1 1 62C28C4A
P 4300 2600
F 0 "R?" H 4370 2646 50  0000 L CNN
F 1 "1k" H 4370 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4230 2600 50  0001 C CNN
F 3 "~" H 4300 2600 50  0001 C CNN
	1    4300 2600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 62C2BAF7
P 4300 3750
F 0 "R?" H 4370 3796 50  0000 L CNN
F 1 "1k" H 4370 3705 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4230 3750 50  0001 C CNN
F 3 "~" H 4300 3750 50  0001 C CNN
	1    4300 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4450 3750 4600 3750
$Comp
L Device:R R?
U 1 1 62C2E53A
P 4300 4900
F 0 "R?" H 4370 4946 50  0000 L CNN
F 1 "1k" H 4370 4855 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4230 4900 50  0001 C CNN
F 3 "~" H 4300 4900 50  0001 C CNN
	1    4300 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4450 4900 4600 4900
$Comp
L Device:R R?
U 1 1 62C2FC66
P 6600 2600
F 0 "R?" H 6670 2646 50  0000 L CNN
F 1 "1k" H 6670 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6530 2600 50  0001 C CNN
F 3 "~" H 6600 2600 50  0001 C CNN
	1    6600 2600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 62C31080
P 6600 3750
F 0 "R?" H 6670 3796 50  0000 L CNN
F 1 "1k" H 6670 3705 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6530 3750 50  0001 C CNN
F 3 "~" H 6600 3750 50  0001 C CNN
	1    6600 3750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 62C31A4A
P 6600 4900
F 0 "R?" H 6670 4946 50  0000 L CNN
F 1 "1k" H 6670 4855 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6530 4900 50  0001 C CNN
F 3 "~" H 6600 4900 50  0001 C CNN
	1    6600 4900
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
