EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 11 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 15250 7770 0    50   ~ 0
MCU_CLK_IN
Wire Notes Line
	13300 8450 15900 8450
Wire Notes Line
	13300 7350 15900 7350
Wire Notes Line
	15900 7350 15900 8450
Wire Notes Line
	13300 7350 13300 8450
$Comp
L power:GND #PWR?
U 1 1 5FC3D4EA
P 13820 8110
AR Path="/5FC3D4EA" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5FC3D4EA" Ref="#PWR0139"  Part="1" 
F 0 "#PWR0139" H 13820 7860 50  0001 C CNN
F 1 "GND" H 13825 7937 50  0000 C CNN
F 2 "" H 13820 8110 50  0001 C CNN
F 3 "" H 13820 8110 50  0001 C CNN
	1    13820 8110
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FC3D4E4
P 13820 7960
AR Path="/5FC3D4E4" Ref="C?"  Part="1" 
AR Path="/5FBFE149/5FC3D4E4" Ref="C39"  Part="1" 
F 0 "C39" H 13935 8006 50  0000 L CNN
F 1 "1n" H 13935 7915 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 13858 7810 50  0001 C CNN
F 3 "~" H 13820 7960 50  0001 C CNN
	1    13820 7960
	1    0    0    -1  
$EndComp
Connection ~ 13850 7770
Wire Wire Line
	13850 7870 13850 7770
Wire Wire Line
	13900 7870 13850 7870
Wire Wire Line
	13850 7770 13850 7670
Wire Wire Line
	13900 7770 13850 7770
Wire Wire Line
	15100 7870 15100 8120
Wire Wire Line
	15050 7870 15100 7870
$Comp
L power:GND #PWR?
U 1 1 5FC3D4D6
P 15100 8120
AR Path="/5FC3D4D6" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5FC3D4D6" Ref="#PWR0137"  Part="1" 
F 0 "#PWR0137" H 15100 7870 50  0001 C CNN
F 1 "GND" H 15105 7947 50  0000 C CNN
F 2 "" H 15100 8120 50  0001 C CNN
F 3 "" H 15100 8120 50  0001 C CNN
	1    15100 8120
	1    0    0    -1  
$EndComp
$Comp
L Components:ECS-TXO-2520-33-120-AN-TR Y?
U 1 1 5FC3D4D0
P 13900 7770
AR Path="/5FC3D4D0" Ref="Y?"  Part="1" 
AR Path="/5FBFE149/5FC3D4D0" Ref="Y1"  Part="1" 
F 0 "Y1" H 14650 8035 50  0000 C CNN
F 1 "ECS-TXO-2520-33-120-AN-TR" H 14650 7944 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_EuroQuartz_X22-4Pin_2.5x2.0mm" H 15250 7870 50  0001 L CNN
F 3 "https://www.ecsxtal.com/store/pdf/ECS-TXO-2520.pdf" H 15250 7770 50  0001 L CNN
F 4 "TCXO Oscillators 12MHz 3.3V 2.5ppm -40C +85C" H 15250 7670 50  0001 L CNN "Description"
F 5 "" H 15250 7570 50  0001 L CNN "Height"
F 6 "520-T2520-33-120-ANT" H 15250 7470 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ECS/ECS-TXO-2520-33-120-AN-TR?qs=KAq9QXcE8HB8zqkkQbQ2Dw%3D%3D" H 15250 7370 50  0001 L CNN "Mouser Price/Stock"
F 8 "ECS" H 15250 7270 50  0001 L CNN "Manufacturer_Name"
F 9 "ECS-TXO-2520-33-120-AN-TR" H 15250 7170 50  0001 L CNN "Manufacturer_Part_Number"
	1    13900 7770
	1    0    0    -1  
$EndComp
Text Notes 14450 7350 0    50   ~ 0
MCU Crystal
Text Notes 14850 8600 0    50   ~ 0
MCU RTC
Wire Notes Line
	14150 8600 15900 8600
Wire Notes Line
	14150 9550 14150 8600
Wire Notes Line
	15900 9550 14150 9550
Wire Notes Line
	15900 8600 15900 9550
Text Label 15350 8850 0    50   ~ 0
MCU_RTC_OUT
Text Label 14650 8850 2    50   ~ 0
MCU_RTC_IN
Wire Wire Line
	15000 9300 15000 9250
Wire Wire Line
	15000 9250 15350 9250
Connection ~ 15000 9250
$Comp
L power:GND #PWR?
U 1 1 5FC3D4B2
P 15000 9300
AR Path="/5FC3D4B2" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5FC3D4B2" Ref="#PWR0138"  Part="1" 
F 0 "#PWR0138" H 15000 9050 50  0001 C CNN
F 1 "GND" H 15005 9127 50  0000 C CNN
F 2 "" H 15000 9300 50  0001 C CNN
F 3 "" H 15000 9300 50  0001 C CNN
	1    15000 9300
	1    0    0    -1  
$EndComp
Connection ~ 15350 8950
Wire Wire Line
	15350 8950 15350 8850
Connection ~ 14650 8950
Wire Wire Line
	14650 8950 14650 8850
Wire Wire Line
	15150 8950 15350 8950
Wire Wire Line
	14650 8950 14850 8950
Wire Wire Line
	14650 9250 15000 9250
$Comp
L Device:C C?
U 1 1 5FC3D4A5
P 14650 9100
AR Path="/5FC3D4A5" Ref="C?"  Part="1" 
AR Path="/5FBFE149/5FC3D4A5" Ref="C36"  Part="1" 
F 0 "C36" H 14765 9146 50  0000 L CNN
F 1 "11p" H 14765 9055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 14688 8950 50  0001 C CNN
F 3 "~" H 14650 9100 50  0001 C CNN
	1    14650 9100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FC3D49F
P 15350 9100
AR Path="/5FC3D49F" Ref="C?"  Part="1" 
AR Path="/5FBFE149/5FC3D49F" Ref="C37"  Part="1" 
F 0 "C37" H 15465 9146 50  0000 L CNN
F 1 "11p" H 15465 9055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 15388 8950 50  0001 C CNN
F 3 "~" H 15350 9100 50  0001 C CNN
	1    15350 9100
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y?
U 1 1 5FC3D499
P 15000 8950
AR Path="/5FC3D499" Ref="Y?"  Part="1" 
AR Path="/5FBFE149/5FC3D499" Ref="Y2"  Part="1" 
F 0 "Y2" H 15000 9218 50  0000 C CNN
F 1 "ECS-.327-7-34B-TR" H 15000 9127 50  0000 C CNN
F 2 "Components:G8327A047" H 15000 8950 50  0001 C CNN
F 3 "~" H 15000 8950 50  0001 C CNN
	1    15000 8950
	1    0    0    -1  
$EndComp
Connection ~ 13500 5150
Wire Wire Line
	13500 5150 13400 5150
Text Label 13400 5150 2    50   ~ 0
MEM_NAND_BUSY_1
$Comp
L Device:R R?
U 1 1 5FC2560E
P 13500 4850
AR Path="/5FC2560E" Ref="R?"  Part="1" 
AR Path="/5FBFE149/5FC2560E" Ref="R89"  Part="1" 
F 0 "R89" H 13570 4896 50  0000 L CNN
F 1 "10k" H 13570 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 13430 4850 50  0001 C CNN
F 3 "~" H 13500 4850 50  0001 C CNN
	1    13500 4850
	-1   0    0    1   
$EndComp
Text Label 13900 5350 2    50   ~ 0
MEM_NAND_ENABLE_1
Text Label 15400 6150 0    50   ~ 0
MEM_D3
Text Label 15400 6250 0    50   ~ 0
MEM_D2
Text Label 15400 6350 0    50   ~ 0
MEM_D1
Text Label 15400 6450 0    50   ~ 0
MEM_D0
Text Label 15400 5250 0    50   ~ 0
MEM_D4
Text Label 15400 5150 0    50   ~ 0
MEM_D5
Text Label 15400 5050 0    50   ~ 0
MEM_D6
Text Label 15400 4950 0    50   ~ 0
MEM_D7
Text Label 13900 5250 2    50   ~ 0
MEM_NAND_OUT_ENABLE
Text Label 13900 6050 2    50   ~ 0
MEM_NAND_CMD_ENABLE
Text Label 13900 6150 2    50   ~ 0
MEM_NAND_ADDR_ENABLE
Text Label 13900 6250 2    50   ~ 0
MEM_NAND_WRT_ENABLE
Text Label 13900 6350 2    50   ~ 0
MEM_NAND_WRT_PROTECT
$Comp
L Components:MT29F64G08AFAAAWP-ITZ_A_TR IC?
U 1 1 5FC255D9
P 13900 4550
AR Path="/5FC255D9" Ref="IC?"  Part="1" 
AR Path="/5FBFE149/5FC255D9" Ref="IC11"  Part="1" 
F 0 "IC11" H 14650 4815 50  0000 C CNN
F 1 "MT29F64G08AFAAAWP-ITZ_A_TR" H 14650 4724 50  0000 C CNN
F 2 "Components:SOP50P2000X120-48N" H 15250 4650 50  0001 L CNN
F 3 "" H 15250 4550 50  0001 L CNN
F 4 "NAND Flash SLC 64G 8GX8 TSOP DDP" H 15250 4450 50  0001 L CNN "Description"
F 5 "1.2" H 15250 4350 50  0001 L CNN "Height"
F 6 "340-200635-REEL" H 15250 4250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Micron/MT29F64G08AFAAAWP-ITZA-TR?qs=taEdVNyAfdHwqoLheszjbg%3D%3D" H 15250 4150 50  0001 L CNN "Mouser Price/Stock"
F 8 "Micron" H 15250 4050 50  0001 L CNN "Manufacturer_Name"
F 9 "MT29F64G08AFAAAWP-ITZ:A TR" H 15250 3950 50  0001 L CNN "Manufacturer_Part_Number"
	1    13900 4550
	1    0    0    -1  
$EndComp
NoConn ~ 15400 4550
NoConn ~ 15400 4650
NoConn ~ 15400 4750
NoConn ~ 15400 4850
NoConn ~ 15400 5350
NoConn ~ 15400 5450
NoConn ~ 15400 5550
NoConn ~ 15400 5850
NoConn ~ 15400 5950
NoConn ~ 15400 6050
NoConn ~ 15400 6550
NoConn ~ 15400 6650
NoConn ~ 15400 6750
NoConn ~ 15400 6850
NoConn ~ 13900 6850
NoConn ~ 13900 6750
NoConn ~ 13900 6650
NoConn ~ 13900 6550
NoConn ~ 13900 6450
NoConn ~ 13900 4550
NoConn ~ 13900 4650
NoConn ~ 13900 4750
NoConn ~ 13900 4850
NoConn ~ 13900 4950
NoConn ~ 13900 5550
NoConn ~ 13900 5850
NoConn ~ 13900 5950
Wire Notes Line
	12550 7200 15900 7200
Wire Notes Line
	15900 7200 15900 3950
Text Notes 14000 3950 0    50   ~ 0
NAND Flash
Wire Notes Line
	12550 3950 12550 7200
Wire Notes Line
	15900 3950 12550 3950
Text GLabel 13850 7670 1    50   Input ~ 0
3.3V
NoConn ~ 1815 2450
$Comp
L Device:C C18
U 1 1 5F6E384D
P 1350 10600
F 0 "C18" H 1465 10646 50  0000 L CNN
F 1 "4.7u" H 1465 10555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1388 10450 50  0001 C CNN
F 3 "~" H 1350 10600 50  0001 C CNN
	1    1350 10600
	1    0    0    -1  
$EndComp
Text GLabel 1250 10450 0    50   Input ~ 0
3.3V
Wire Wire Line
	1250 10450 1350 10450
Wire Wire Line
	1350 10450 1450 10450
Connection ~ 1350 10450
Text Label 1450 10450 0    50   ~ 0
MCU_VDD
$Comp
L power:GND #PWR0120
U 1 1 5F6E6764
P 1350 10750
F 0 "#PWR0120" H 1350 10500 50  0001 C CNN
F 1 "GND" H 1355 10577 50  0000 C CNN
F 2 "" H 1350 10750 50  0001 C CNN
F 3 "" H 1350 10750 50  0001 C CNN
	1    1350 10750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C20
U 1 1 5F6E808B
P 2400 10550
F 0 "C20" H 2515 10596 50  0000 L CNN
F 1 "0.1u" H 2515 10505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2438 10400 50  0001 C CNN
F 3 "~" H 2400 10550 50  0001 C CNN
	1    2400 10550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C22
U 1 1 5F6E871D
P 2800 10550
F 0 "C22" H 2915 10596 50  0000 L CNN
F 1 "0.1u" H 2915 10505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2838 10400 50  0001 C CNN
F 3 "~" H 2800 10550 50  0001 C CNN
	1    2800 10550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C24
U 1 1 5F6E8A01
P 3200 10550
F 0 "C24" H 3315 10596 50  0000 L CNN
F 1 "0.1u" H 3315 10505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3238 10400 50  0001 C CNN
F 3 "~" H 3200 10550 50  0001 C CNN
	1    3200 10550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C26
U 1 1 5F6E8C88
P 3600 10550
F 0 "C26" H 3715 10596 50  0000 L CNN
F 1 "0.1u" H 3715 10505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3638 10400 50  0001 C CNN
F 3 "~" H 3600 10550 50  0001 C CNN
	1    3600 10550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C28
U 1 1 5F6E903C
P 4000 10550
F 0 "C28" H 4115 10596 50  0000 L CNN
F 1 "0.1u" H 4115 10505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4038 10400 50  0001 C CNN
F 3 "~" H 4000 10550 50  0001 C CNN
	1    4000 10550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 10700 2400 10750
Wire Wire Line
	4000 10750 4000 10700
Wire Wire Line
	3600 10700 3600 10750
Connection ~ 3600 10750
Wire Wire Line
	3600 10750 4000 10750
Wire Wire Line
	2400 10750 2800 10750
Wire Wire Line
	3200 10700 3200 10750
Connection ~ 3200 10750
Wire Wire Line
	3200 10750 3600 10750
Wire Wire Line
	2800 10700 2800 10750
Connection ~ 2800 10750
Wire Wire Line
	2800 10750 3200 10750
$Comp
L power:GND #PWR0123
U 1 1 5F6EF9A6
P 3200 10750
F 0 "#PWR0123" H 3200 10500 50  0001 C CNN
F 1 "GND" H 3205 10577 50  0000 C CNN
F 2 "" H 3200 10750 50  0001 C CNN
F 3 "" H 3200 10750 50  0001 C CNN
	1    3200 10750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 10400 2800 10400
Connection ~ 2800 10400
Wire Wire Line
	2800 10400 3200 10400
Connection ~ 3200 10400
Wire Wire Line
	3200 10400 3600 10400
Connection ~ 3600 10400
Wire Wire Line
	3600 10400 4000 10400
Text Label 4000 10400 0    50   ~ 0
MCU_VDDIO
Text Label 2400 10400 2    50   ~ 0
MCU_VDD
$Comp
L Device:C C19
U 1 1 5F6F7B66
P 2300 9650
F 0 "C19" H 2415 9696 50  0000 L CNN
F 1 "0.1u" H 2415 9605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2338 9500 50  0001 C CNN
F 3 "~" H 2300 9650 50  0001 C CNN
	1    2300 9650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C21
U 1 1 5F6F7B6C
P 2700 9650
F 0 "C21" H 2815 9696 50  0000 L CNN
F 1 "0.1u" H 2815 9605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2738 9500 50  0001 C CNN
F 3 "~" H 2700 9650 50  0001 C CNN
	1    2700 9650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C23
U 1 1 5F6F7B72
P 3100 9650
F 0 "C23" H 3215 9696 50  0000 L CNN
F 1 "0.1u" H 3215 9605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3138 9500 50  0001 C CNN
F 3 "~" H 3100 9650 50  0001 C CNN
	1    3100 9650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C25
U 1 1 5F6F7B78
P 3500 9650
F 0 "C25" H 3615 9696 50  0000 L CNN
F 1 "0.1u" H 3615 9605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3538 9500 50  0001 C CNN
F 3 "~" H 3500 9650 50  0001 C CNN
	1    3500 9650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C27
U 1 1 5F6F7B7E
P 3900 9650
F 0 "C27" H 4015 9696 50  0000 L CNN
F 1 "0.1u" H 4015 9605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3938 9500 50  0001 C CNN
F 3 "~" H 3900 9650 50  0001 C CNN
	1    3900 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 9800 2300 9850
Wire Wire Line
	3900 9850 3900 9800
Wire Wire Line
	3500 9800 3500 9850
Connection ~ 3500 9850
Wire Wire Line
	3500 9850 3900 9850
Wire Wire Line
	2300 9850 2700 9850
Wire Wire Line
	3100 9800 3100 9850
Connection ~ 3100 9850
Wire Wire Line
	3100 9850 3500 9850
Wire Wire Line
	2700 9800 2700 9850
Connection ~ 2700 9850
Wire Wire Line
	2700 9850 3100 9850
$Comp
L power:GND #PWR0122
U 1 1 5F6F7B90
P 3100 9850
F 0 "#PWR0122" H 3100 9600 50  0001 C CNN
F 1 "GND" H 3105 9677 50  0000 C CNN
F 2 "" H 3100 9850 50  0001 C CNN
F 3 "" H 3100 9850 50  0001 C CNN
	1    3100 9850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 9500 2700 9500
Connection ~ 2700 9500
Wire Wire Line
	2700 9500 3100 9500
Connection ~ 3100 9500
Wire Wire Line
	3100 9500 3500 9500
Connection ~ 3500 9500
Wire Wire Line
	3500 9500 3900 9500
Text Label 3900 9500 0    50   ~ 0
MCU_VDDCORE
$Comp
L Device:C C17
U 1 1 5F6F9AFD
P 1350 9650
F 0 "C17" H 1465 9696 50  0000 L CNN
F 1 "0.1u" H 1465 9605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1388 9500 50  0001 C CNN
F 3 "~" H 1350 9650 50  0001 C CNN
	1    1350 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 9500 1350 9500
Wire Wire Line
	1350 9500 1450 9500
Connection ~ 1350 9500
Text Label 1450 9500 0    50   ~ 0
MCU_VDDIN
$Comp
L power:GND #PWR0119
U 1 1 5F6F9B07
P 1350 9800
F 0 "#PWR0119" H 1350 9550 50  0001 C CNN
F 1 "GND" H 1355 9627 50  0000 C CNN
F 2 "" H 1350 9800 50  0001 C CNN
F 3 "" H 1350 9800 50  0001 C CNN
	1    1350 9800
	1    0    0    -1  
$EndComp
Text Label 1250 9500 2    50   ~ 0
MCU_VDD
Text Notes 2300 9350 0    50   ~ 0
MCU core power (VDDCORE_x - 1 for each pin)
Wire Notes Line
	4500 9350 4500 10100
Wire Notes Line
	4500 10100 1900 10100
Wire Notes Line
	1900 10100 1900 9350
Wire Notes Line
	1900 9350 4500 9350
Text Notes 2450 10250 0    50   ~ 0
MCU IO power (VDDIO_x - 1 for each pin)
Wire Notes Line
	4450 10250 4450 11000
Wire Notes Line
	4450 11000 2000 11000
Wire Notes Line
	2000 11000 2000 10250
Wire Notes Line
	2000 10250 4450 10250
$Comp
L Device:Ferrite_Bead FB1
U 1 1 5F791062
P 5100 9400
F 0 "FB1" V 5374 9400 50  0000 C CNN
F 1 "470 (100MHz)" V 5283 9400 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" V 5030 9400 50  0001 C CNN
F 3 "~" H 5100 9400 50  0001 C CNN
	1    5100 9400
	0    -1   -1   0   
$EndComp
Text GLabel 4950 9400 0    50   Input ~ 0
3.3V
$Comp
L Device:C C31
U 1 1 5F79202E
P 5350 9600
F 0 "C31" H 5465 9646 50  0000 L CNN
F 1 "0.1u" H 5465 9555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5388 9450 50  0001 C CNN
F 3 "~" H 5350 9600 50  0001 C CNN
	1    5350 9600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F79286E
P 5350 9850
AR Path="/5F79286E" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5F79286E" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 5350 9600 50  0001 C CNN
F 1 "GND" H 5355 9677 50  0000 C CNN
F 2 "" H 5350 9850 50  0001 C CNN
F 3 "" H 5350 9850 50  0001 C CNN
	1    5350 9850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 9750 5350 9800
Wire Wire Line
	5250 9400 5350 9400
Wire Wire Line
	5350 9400 5350 9450
Wire Wire Line
	5350 9400 5400 9400
Connection ~ 5350 9400
Wire Wire Line
	5350 9800 5400 9800
Connection ~ 5350 9800
Wire Wire Line
	5350 9800 5350 9850
Text Label 5400 9400 0    50   ~ 0
MCU_AD_REF_POS
Text Label 5400 9800 0    50   ~ 0
MCU_AD_REF_NEG
Text HLabel 8400 9400 3    50   Output ~ 0
SPI_SCK_1
Text HLabel 8500 9400 3    50   Output ~ 0
SPI_MOSI_1
Text HLabel 8600 9400 3    50   Input ~ 0
SPI_MISO_1
Text HLabel 8750 9400 3    50   Output ~ 0
SPI_SCK_2
Text HLabel 8850 9400 3    50   Output ~ 0
SPI_MOSI_2
Text HLabel 8950 9400 3    50   Input ~ 0
SPI_MISO_2
Entry Wire Line
	8400 9000 8500 8900
Entry Wire Line
	8500 9000 8600 8900
Entry Wire Line
	8600 9000 8700 8900
Entry Wire Line
	8750 9000 8850 8900
Entry Wire Line
	8850 9000 8950 8900
Entry Wire Line
	8950 9000 9050 8900
Wire Wire Line
	8400 9000 8400 9400
Wire Wire Line
	8500 9000 8500 9400
Wire Wire Line
	8600 9000 8600 9400
Wire Wire Line
	8750 9000 8750 9400
Wire Wire Line
	8850 9000 8850 9400
Wire Wire Line
	8950 9000 8950 9400
Text Label 8400 9050 3    50   ~ 0
SPI_SCK
Text Label 8500 9050 3    50   ~ 0
SPI_MOSI
Text Label 8600 9050 3    50   ~ 0
SPI_MISO
Text Label 8750 9050 3    50   ~ 0
SPI_SCK
Text Label 8850 9050 3    50   ~ 0
SPI_MOSI
Text Label 8950 9050 3    50   ~ 0
SPI_MISO
Text Notes 1700 2800 0    50   ~ 0
LEDs
Wire Notes Line
	750  3450 2850 3450
Wire Wire Line
	1400 3150 1550 3150
Text Label 1400 3150 2    50   ~ 0
DBG_LED
Wire Wire Line
	2300 3150 2200 3150
$Comp
L power:GND #PWR?
U 1 1 5F9631F8
P 2300 3150
AR Path="/5F9631F8" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5F9631F8" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 2300 2900 50  0001 C CNN
F 1 "GND" V 2305 3022 50  0000 R CNN
F 2 "" H 2300 3150 50  0001 C CNN
F 3 "" H 2300 3150 50  0001 C CNN
	1    2300 3150
	0    -1   -1   0   
$EndComp
$Comp
L Components:LED D?
U 1 1 5F9631FE
P 2050 3150
AR Path="/5F9631FE" Ref="D?"  Part="1" 
AR Path="/5FBFE149/5F9631FE" Ref="D1"  Part="1" 
F 0 "D1" H 2043 3366 50  0000 C CNN
F 1 "LED" H 2043 3275 50  0000 C CNN
F 2 "Components:LED_0402_1005Metric" H 2050 3150 50  0001 C CNN
F 3 "~" H 2050 3150 50  0001 C CNN
	1    2050 3150
	-1   0    0    1   
$EndComp
Wire Notes Line
	2850 3450 2850 2800
Wire Notes Line
	2850 2800 750  2800
Wire Notes Line
	750  3450 750  2800
Wire Wire Line
	13500 5150 13900 5150
Wire Wire Line
	13900 5050 13800 5050
$Comp
L Device:R R?
U 1 1 5FA0DE5D
P 13800 4850
AR Path="/5FA0DE5D" Ref="R?"  Part="1" 
AR Path="/5FBFE149/5FA0DE5D" Ref="R90"  Part="1" 
F 0 "R90" H 13870 4896 50  0000 L CNN
F 1 "10k" H 13870 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 13730 4850 50  0001 C CNN
F 3 "~" H 13800 4850 50  0001 C CNN
	1    13800 4850
	-1   0    0    1   
$EndComp
Wire Wire Line
	13800 5000 13800 5050
Connection ~ 13800 5050
Text Label 13700 5050 2    50   ~ 0
MEM_NAND_BUSY_2
Wire Wire Line
	13700 5050 13800 5050
$Comp
L power:GND #PWR?
U 1 1 5FA1F0EC
P 13800 5750
AR Path="/5FA1F0EC" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5FA1F0EC" Ref="#PWR0136"  Part="1" 
F 0 "#PWR0136" H 13800 5500 50  0001 C CNN
F 1 "GND" H 13805 5577 50  0000 C CNN
F 2 "" H 13800 5750 50  0001 C CNN
F 3 "" H 13800 5750 50  0001 C CNN
	1    13800 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	13900 5750 13800 5750
$Comp
L Device:R R?
U 1 1 5FA35509
P 13000 4650
AR Path="/5FA35509" Ref="R?"  Part="1" 
AR Path="/5FBFE149/5FA35509" Ref="R87"  Part="1" 
F 0 "R87" H 13070 4696 50  0000 L CNN
F 1 "10k" H 13070 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 12930 4650 50  0001 C CNN
F 3 "~" H 13000 4650 50  0001 C CNN
	1    13000 4650
	-1   0    0    1   
$EndComp
Text Label 13900 5450 2    50   ~ 0
MEM_NAND_ENABLE_2
$Comp
L Device:R R?
U 1 1 5FA498EF
P 13300 4650
AR Path="/5FA498EF" Ref="R?"  Part="1" 
AR Path="/5FBFE149/5FA498EF" Ref="R88"  Part="1" 
F 0 "R88" H 13370 4696 50  0000 L CNN
F 1 "10k" H 13370 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 13230 4650 50  0001 C CNN
F 3 "~" H 13300 4650 50  0001 C CNN
	1    13300 4650
	-1   0    0    1   
$EndComp
Wire Wire Line
	13000 5450 13000 4800
Wire Wire Line
	13000 5450 13900 5450
Wire Wire Line
	13300 5350 13300 4800
Wire Wire Line
	13300 5350 13900 5350
Wire Wire Line
	13500 5000 13500 5150
Wire Wire Line
	13800 4700 13800 4650
Wire Wire Line
	13800 4650 13650 4650
Wire Wire Line
	13500 4650 13500 4700
Wire Wire Line
	13000 4500 13000 4450
Wire Wire Line
	13000 4450 13150 4450
Wire Wire Line
	13300 4450 13300 4500
Wire Wire Line
	13650 4650 13650 4400
Wire Wire Line
	13650 4400 13400 4400
Wire Wire Line
	13150 4400 13150 4450
Connection ~ 13650 4650
Wire Wire Line
	13650 4650 13500 4650
Connection ~ 13150 4450
Wire Wire Line
	13150 4450 13300 4450
Wire Wire Line
	13400 4350 13400 4400
Connection ~ 13400 4400
Wire Wire Line
	13400 4400 13150 4400
$Comp
L power:GND #PWR?
U 1 1 5FAAFE9D
P 15500 5750
AR Path="/5FAAFE9D" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5FAAFE9D" Ref="#PWR0140"  Part="1" 
F 0 "#PWR0140" H 15500 5500 50  0001 C CNN
F 1 "GND" H 15505 5577 50  0000 C CNN
F 2 "" H 15500 5750 50  0001 C CNN
F 3 "" H 15500 5750 50  0001 C CNN
	1    15500 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	15400 5750 15500 5750
Text GLabel 1315 1250 0    50   Input ~ 0
3.3V
Text GLabel 2315 1250 2    50   Input ~ 0
5V
Text Label 2315 2150 0    50   ~ 0
MCU_SWDIO
Text Label 2315 1750 0    50   ~ 0
MCU_SWCLK
$Comp
L power:GND #PWR?
U 1 1 5FB61A76
P 1265 1700
AR Path="/5FB61A76" Ref="#PWR?"  Part="1" 
AR Path="/5FB89B52/5FB61A76" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5FB61A76" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 1265 1450 50  0001 C CNN
F 1 "GND" H 1270 1527 50  0000 C CNN
F 2 "" H 1265 1700 50  0001 C CNN
F 3 "" H 1265 1700 50  0001 C CNN
	1    1265 1700
	0    1    -1   0   
$EndComp
Text Label 1315 1950 2    50   ~ 0
I2C_SDA_1
Text Label 1315 1850 2    50   ~ 0
I2C_SCL_1
Text Label 1315 1550 2    50   ~ 0
I2C_SDA_2
Text Label 1315 2050 2    50   ~ 0
I2C_SCL_2
Text Label 2315 1450 0    50   ~ 0
SPI_SCK
Text Label 2315 1850 0    50   ~ 0
SPI_MOSI
Text Label 2315 1650 0    50   ~ 0
SPI_MISO
Text Label 1315 1350 2    50   ~ 0
SPARE_GPIO_1
Text Label 2315 1350 0    50   ~ 0
SPARE_GPIO_2
Text Label 2315 2050 0    50   ~ 0
SPARE_GPIO_3
Text Label 2315 1550 0    50   ~ 0
SPI_NSS_1
Text Label 2315 1950 0    50   ~ 0
SPI_NSS_2
Wire Notes Line
	2850 900  2850 2650
Wire Notes Line
	2850 2650 750  2650
Wire Notes Line
	750  2650 750  900 
Wire Notes Line
	750  900  2850 900 
Text Notes 1400 900  0    50   ~ 0
Debugging connector
Text Label 1315 1450 2    50   ~ 0
MCU_NRST
$Comp
L Device:Ferrite_Bead FB2
U 1 1 5FCE2C07
P 5250 10450
F 0 "FB2" V 5524 10450 50  0000 C CNN
F 1 "470 (100MHz)" V 5433 10450 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" V 5180 10450 50  0001 C CNN
F 3 "~" H 5250 10450 50  0001 C CNN
	1    5250 10450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C32
U 1 1 5FCE300B
P 5500 10650
F 0 "C32" H 5615 10696 50  0000 L CNN
F 1 "0.1u" H 5615 10605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5538 10500 50  0001 C CNN
F 3 "~" H 5500 10650 50  0001 C CNN
	1    5500 10650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 10450 5500 10450
Wire Wire Line
	5500 10450 5500 10500
$Comp
L power:GND #PWR?
U 1 1 5FCEA2E5
P 5500 10850
AR Path="/5FCEA2E5" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5FCEA2E5" Ref="#PWR0126"  Part="1" 
F 0 "#PWR0126" H 5500 10600 50  0001 C CNN
F 1 "GND" H 5505 10677 50  0000 C CNN
F 2 "" H 5500 10850 50  0001 C CNN
F 3 "" H 5500 10850 50  0001 C CNN
	1    5500 10850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 10800 5500 10850
Wire Wire Line
	5500 10450 5600 10450
Connection ~ 5500 10450
$Comp
L Device:C C33
U 1 1 5FCFFF03
P 7100 9650
F 0 "C33" H 7215 9696 50  0000 L CNN
F 1 "0.1u" H 7215 9605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7138 9500 50  0001 C CNN
F 3 "~" H 7100 9650 50  0001 C CNN
	1    7100 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 9450 7100 9450
Wire Wire Line
	7100 9450 7100 9500
Wire Wire Line
	7100 9450 7050 9450
Connection ~ 7100 9450
Text Label 7050 9450 2    50   ~ 0
MCU_VDD
$Comp
L power:GND #PWR?
U 1 1 5FD0EBDC
P 7100 9850
AR Path="/5FD0EBDC" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5FD0EBDC" Ref="#PWR0130"  Part="1" 
F 0 "#PWR0130" H 7100 9600 50  0001 C CNN
F 1 "GND" H 7105 9677 50  0000 C CNN
F 2 "" H 7100 9850 50  0001 C CNN
F 3 "" H 7100 9850 50  0001 C CNN
	1    7100 9850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 9800 7100 9850
Text Label 5600 10450 0    50   ~ 0
MCU_VDDUTMIC
Text Label 7150 9450 0    50   ~ 0
MCU_VDDUTMII
Text HLabel 1315 2150 0    50   Input ~ 0
FGD_VB
Wire Wire Line
	1315 1650 1315 1700
Wire Wire Line
	1315 1700 1265 1700
Connection ~ 1315 1700
Wire Wire Line
	1315 1700 1315 1750
$Comp
L Device:R R85
U 1 1 5FED78E7
P 1700 3150
F 0 "R85" V 1493 3150 50  0000 C CNN
F 1 "470" V 1584 3150 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" V 1630 3150 50  0001 C CNN
F 3 "~" H 1700 3150 50  0001 C CNN
	1    1700 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	1900 3150 1850 3150
Text Label 3100 9500 2    50   ~ 0
MCU_VDD_OUT
$Comp
L Device:Ferrite_Bead FB3
U 1 1 5FF3F064
P 6900 10450
F 0 "FB3" V 7174 10450 50  0000 C CNN
F 1 "470 (100MHz)" V 7083 10450 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" V 6830 10450 50  0001 C CNN
F 3 "~" H 6900 10450 50  0001 C CNN
	1    6900 10450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C34
U 1 1 5FF3F06A
P 7100 10650
F 0 "C34" H 7215 10696 50  0000 L CNN
F 1 "0.1u" H 7215 10605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7138 10500 50  0001 C CNN
F 3 "~" H 7100 10650 50  0001 C CNN
	1    7100 10650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 10450 7100 10500
$Comp
L power:GND #PWR?
U 1 1 5FF3F071
P 7100 10850
AR Path="/5FF3F071" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5FF3F071" Ref="#PWR0131"  Part="1" 
F 0 "#PWR0131" H 7100 10600 50  0001 C CNN
F 1 "GND" H 7105 10677 50  0000 C CNN
F 2 "" H 7100 10850 50  0001 C CNN
F 3 "" H 7100 10850 50  0001 C CNN
	1    7100 10850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 10850 7100 10800
Wire Wire Line
	7050 10450 7100 10450
Text Label 6750 10450 2    50   ~ 0
MCU_VDD_OUT
Text Label 5100 10450 2    50   ~ 0
MCU_VDD_OUT
Wire Wire Line
	7100 10450 7200 10450
Connection ~ 7100 10450
Text Label 7200 10450 0    50   ~ 0
MCU_VDDPLL
$Comp
L Device:C C35
U 1 1 5FFEDE19
P 9100 10650
F 0 "C35" H 9215 10696 50  0000 L CNN
F 1 "4.7u" H 9215 10605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9138 10500 50  0001 C CNN
F 3 "~" H 9100 10650 50  0001 C CNN
	1    9100 10650
	1    0    0    -1  
$EndComp
$Comp
L pspice:INDUCTOR L1
U 1 1 5FFEE8B6
P 8750 10500
F 0 "L1" H 8750 10715 50  0000 C CNN
F 1 "10uH - 60mA" H 8750 10624 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" H 8750 10500 50  0001 C CNN
F 3 "~" H 8750 10500 50  0001 C CNN
	1    8750 10500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 10500 9100 10500
$Comp
L Device:R R86
U 1 1 5FFFC470
P 8250 10500
F 0 "R86" V 8043 10500 50  0000 C CNN
F 1 "2.2" V 8134 10500 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" V 8180 10500 50  0001 C CNN
F 3 "~" H 8250 10500 50  0001 C CNN
	1    8250 10500
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 10500 8500 10500
$Comp
L power:GND #PWR?
U 1 1 60003AF2
P 9100 10850
AR Path="/60003AF2" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/60003AF2" Ref="#PWR0134"  Part="1" 
F 0 "#PWR0134" H 9100 10600 50  0001 C CNN
F 1 "GND" H 9105 10677 50  0000 C CNN
F 2 "" H 9100 10850 50  0001 C CNN
F 3 "" H 9100 10850 50  0001 C CNN
	1    9100 10850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 10850 9100 10800
Text Label 8100 10500 2    50   ~ 0
MCU_VDD
Text Label 9100 10500 0    50   ~ 0
MCU_VDDPLLUSB
Text Label 6200 2700 1    50   ~ 0
MCU_VDDPLLUSB
Wire Wire Line
	4350 3900 4500 3900
Connection ~ 4350 3900
Wire Wire Line
	4350 3800 4350 3900
Wire Wire Line
	5800 3800 4350 3800
Wire Wire Line
	4500 3900 4500 3950
Wire Wire Line
	4150 3900 4350 3900
Wire Wire Line
	4150 3950 4150 3900
Wire Wire Line
	4350 4300 4500 4300
Connection ~ 4350 4300
Wire Wire Line
	4500 4300 4500 4250
Wire Wire Line
	4150 4300 4350 4300
Wire Wire Line
	4150 4250 4150 4300
$Comp
L power:GND #PWR?
U 1 1 5FF7F2D7
P 4350 4300
AR Path="/5FF7F2D7" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5FF7F2D7" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 4350 4050 50  0001 C CNN
F 1 "GND" H 4355 4127 50  0000 C CNN
F 2 "" H 4350 4300 50  0001 C CNN
F 3 "" H 4350 4300 50  0001 C CNN
	1    4350 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C29
U 1 1 5FF7F2D1
P 4150 4100
F 0 "C29" H 4265 4146 50  0000 L CNN
F 1 "1u" H 4265 4055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4188 3950 50  0001 C CNN
F 3 "~" H 4150 4100 50  0001 C CNN
	1    4150 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C30
U 1 1 5FF7F2CB
P 4500 4100
F 0 "C30" H 4615 4146 50  0000 L CNN
F 1 "0.1u" H 4615 4055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4538 3950 50  0001 C CNN
F 3 "~" H 4500 4100 50  0001 C CNN
	1    4500 4100
	1    0    0    -1  
$EndComp
Text Label 5300 3800 2    50   ~ 0
MCU_VDD_OUT
Text Label 6600 2700 1    50   ~ 0
MCU_VDDUTMIC
Text Label 7100 2700 1    50   ~ 0
MCU_VDDUTMII
NoConn ~ 6500 2700
NoConn ~ 8400 2700
NoConn ~ 9900 4000
Text Label 9900 6100 0    50   ~ 0
MCU_NRST
Text Label 9900 5500 0    50   ~ 0
MCU_SWCLK
Text Label 9900 6500 0    50   ~ 0
MCU_SWDIO
Text Label 9900 6800 0    50   ~ 0
MEM_NAND_ENABLE_2
Text HLabel 11800 5400 2    50   BiDi ~ 0
I2C_SDA_HUM_1
Text HLabel 11800 5500 2    50   Output ~ 0
I2C_SCL_HUM_1
Text HLabel 11800 5650 2    50   BiDi ~ 0
I2C_SDA_TMP_1
Text HLabel 11800 5750 2    50   Output ~ 0
I2C_SCL_TMP_1
Text HLabel 11800 6000 2    50   Output ~ 0
I2C_SCL_FG
Text HLabel 11800 5900 2    50   BiDi ~ 0
I2C_SDA_FG
Entry Wire Line
	11150 5500 11250 5400
Entry Wire Line
	11150 5600 11250 5500
Entry Wire Line
	11150 5750 11250 5650
Entry Wire Line
	11150 5850 11250 5750
Entry Wire Line
	11150 6000 11250 5900
Entry Wire Line
	11150 6100 11250 6000
Text Label 11350 5400 0    50   ~ 0
I2C_SDA_1
Wire Wire Line
	11250 5400 11800 5400
Wire Wire Line
	11250 5500 11800 5500
Wire Wire Line
	11250 5650 11800 5650
Wire Wire Line
	11250 5750 11800 5750
Text Label 11350 5500 0    50   ~ 0
I2C_SCL_1
Text Label 11350 5650 0    50   ~ 0
I2C_SDA_1
Text Label 11350 5750 0    50   ~ 0
I2C_SCL_1
Wire Wire Line
	11250 5900 11800 5900
Wire Wire Line
	11250 6000 11800 6000
Text Label 11350 5900 0    50   ~ 0
I2C_SDA_1
Text Label 11350 6000 0    50   ~ 0
I2C_SCL_1
Entry Wire Line
	11050 6700 11150 6600
Entry Wire Line
	11050 5300 11150 5400
Wire Wire Line
	9900 6700 11050 6700
Wire Wire Line
	11050 5300 9900 5300
Text Label 9900 4600 0    50   ~ 0
MEM_NAND_BUSY_2
Text HLabel 3000 6300 1    50   Output ~ 0
CONTROL_PUMP_PWM_A1
Text HLabel 2900 6300 1    50   Output ~ 0
CONTROL_PUMP_PWM_A2
Text HLabel 2700 7700 3    50   Output ~ 0
FLOW_PUMP_PWM_B1
Text HLabel 2900 7700 3    50   Output ~ 0
FLOW_PUMP_PWM_A1
Text HLabel 3000 7700 3    50   Output ~ 0
FLOW_PUMP_PWM_A2
Text HLabel 8600 8000 3    50   Output ~ 0
FLOW_VALVE_8_CLOSE
Text HLabel 8800 8000 3    50   Output ~ 0
FLOW_VALVE_8_OPEN
Text HLabel 9900 6200 2    50   Output ~ 0
FLOW_VALVE_7_CLOSE
Text HLabel 9900 6000 2    50   Output ~ 0
FLOW_VALVE_7_OPEN
Text HLabel 9900 5700 2    50   Output ~ 0
FLOW_VALVE_6_CLOSE
Text HLabel 9900 5600 2    50   Output ~ 0
FLOW_VALVE_6_OPEN
Text HLabel 6900 8000 3    50   Output ~ 0
FLOW_VALVE_4_CLOSE
Text HLabel 7000 8000 3    50   Output ~ 0
FLOW_VALVE_4_OPEN
Text HLabel 5800 4500 0    50   Output ~ 0
FLOW_VALVE_2_CLOSE
Text HLabel 5800 4700 0    50   Output ~ 0
FLOW_VALVE_2_OPEN
Text HLabel 5800 4200 0    50   Output ~ 0
FLOW_VALVE_1_CLOSE
Text HLabel 8800 2700 1    50   Output ~ 0
LED_4_SIGNAL
Text HLabel 6800 2700 1    50   BiDi ~ 0
USB_D+
Text HLabel 6900 2700 1    50   BiDi ~ 0
USB_D-
Wire Wire Line
	8900 8800 8900 8000
Wire Wire Line
	8700 8800 8700 8000
Wire Wire Line
	8400 8800 8400 8000
Entry Wire Line
	8900 8800 9000 8900
Entry Wire Line
	8700 8800 8800 8900
Entry Wire Line
	8400 8800 8500 8900
Text Label 9600 2700 1    50   ~ 0
I2C_SCL_2
Text Label 10200 3900 0    50   ~ 0
I2C_SDA_2
Wire Wire Line
	11050 3900 9900 3900
Wire Wire Line
	9600 2300 9600 2700
Entry Wire Line
	9700 2200 9600 2300
Entry Wire Line
	11050 3900 11150 3800
Text Label 11350 3200 0    50   ~ 0
I2C_SCL_2
Text Label 11350 3100 0    50   ~ 0
I2C_SDA_2
Text Label 11350 2950 0    50   ~ 0
I2C_SCL_2
Wire Wire Line
	11250 3200 11800 3200
Wire Wire Line
	11250 3100 11800 3100
Wire Wire Line
	11250 2950 11800 2950
Wire Wire Line
	11250 2850 11800 2850
Text Label 11350 2850 0    50   ~ 0
I2C_SDA_2
Entry Wire Line
	11150 3300 11250 3200
Entry Wire Line
	11150 3200 11250 3100
Entry Wire Line
	11150 3050 11250 2950
Entry Wire Line
	11150 2950 11250 2850
Text HLabel 11800 3200 2    50   Output ~ 0
I2C_SCL_TMP_2
Text HLabel 11800 3100 2    50   BiDi ~ 0
I2C_SDA_TMP_2
Text HLabel 11800 2950 2    50   Output ~ 0
I2C_SCL_HUM_2
Text HLabel 11800 2850 2    50   BiDi ~ 0
I2C_SDA_HUM_2
Text Label 10250 6700 0    50   ~ 0
I2C_SCL_1
Text Label 10250 5300 0    50   ~ 0
I2C_SDA_1
Text HLabel 8700 2700 1    50   Output ~ 0
SPI_NSS_2
Text HLabel 9900 4200 2    50   Output ~ 0
HUMIDITY_RST2
Text HLabel 9900 6600 2    50   Output ~ 0
HUMIDITY_RST1
Text HLabel 5800 6900 0    50   Input ~ 0
CHIP_TEMP_3_IN
Text HLabel 5800 6700 0    50   Input ~ 0
CHIP_TEMP_2_IN
Text HLabel 5800 6300 0    50   Input ~ 0
CHIP_TEMP_1_IN
Text Label 5800 4400 2    50   ~ 0
MCU_AD_REF_POS
Text Label 5800 4300 2    50   ~ 0
MCU_AD_REF_NEG
Text Label 5800 4000 2    50   ~ 0
MCU_VDDIN
Wire Wire Line
	10400 4900 9900 4900
$Comp
L power:GND #PWR?
U 1 1 5F70D59D
P 10400 4900
AR Path="/5F70D59D" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5F70D59D" Ref="#PWR0135"  Part="1" 
F 0 "#PWR0135" H 10400 4650 50  0001 C CNN
F 1 "GND" H 10405 4727 50  0000 C CNN
F 2 "" H 10400 4900 50  0001 C CNN
F 3 "" H 10400 4900 50  0001 C CNN
	1    10400 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9000 2400 9000 2700
$Comp
L power:GND #PWR?
U 1 1 5F70A37C
P 9000 2400
AR Path="/5F70A37C" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5F70A37C" Ref="#PWR0133"  Part="1" 
F 0 "#PWR0133" H 9000 2150 50  0001 C CNN
F 1 "GND" H 9005 2227 50  0000 C CNN
F 2 "" H 9000 2400 50  0001 C CNN
F 3 "" H 9000 2400 50  0001 C CNN
	1    9000 2400
	-1   0    0    1   
$EndComp
Wire Wire Line
	7000 2400 7000 2700
$Comp
L power:GND #PWR?
U 1 1 5F7077BC
P 7000 2400
AR Path="/5F7077BC" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5F7077BC" Ref="#PWR0129"  Part="1" 
F 0 "#PWR0129" H 7000 2150 50  0001 C CNN
F 1 "GND" H 7005 2227 50  0000 C CNN
F 2 "" H 7000 2400 50  0001 C CNN
F 3 "" H 7000 2400 50  0001 C CNN
	1    7000 2400
	-1   0    0    1   
$EndComp
Wire Wire Line
	6700 2400 6700 2700
$Comp
L power:GND #PWR?
U 1 1 5F70349C
P 6700 2400
AR Path="/5F70349C" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5F70349C" Ref="#PWR0127"  Part="1" 
F 0 "#PWR0127" H 6700 2150 50  0001 C CNN
F 1 "GND" H 6705 2227 50  0000 C CNN
F 2 "" H 6700 2400 50  0001 C CNN
F 3 "" H 6700 2400 50  0001 C CNN
	1    6700 2400
	-1   0    0    1   
$EndComp
Text Label 9900 3700 0    50   ~ 0
MCU_VDDCORE
Text Label 9900 6400 0    50   ~ 0
MCU_VDDIO
Text Label 9900 4800 0    50   ~ 0
MCU_VDDIO
Text Label 5800 6500 2    50   ~ 0
MCU_VDDIO
Text Label 9900 6300 0    50   ~ 0
MCU_VDDCORE
Text Label 5800 6400 2    50   ~ 0
MCU_VDDCORE
Text Label 5800 6800 2    50   ~ 0
MCU_VDDCORE
Text Label 7400 8000 3    50   ~ 0
MCU_VDDCORE
Wire Wire Line
	6800 8100 6800 8000
Wire Wire Line
	8500 8100 8500 8000
$Comp
L power:GND #PWR?
U 1 1 5F6FCD38
P 8500 8100
AR Path="/5F6FCD38" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5F6FCD38" Ref="#PWR0132"  Part="1" 
F 0 "#PWR0132" H 8500 7850 50  0001 C CNN
F 1 "GND" H 8505 7927 50  0000 C CNN
F 2 "" H 8500 8100 50  0001 C CNN
F 3 "" H 8500 8100 50  0001 C CNN
	1    8500 8100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6FC704
P 6800 8100
AR Path="/5F6FC704" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/5F6FC704" Ref="#PWR0128"  Part="1" 
F 0 "#PWR0128" H 6800 7850 50  0001 C CNN
F 1 "GND" H 6805 7927 50  0000 C CNN
F 2 "" H 6800 8100 50  0001 C CNN
F 3 "" H 6800 8100 50  0001 C CNN
	1    6800 8100
	1    0    0    -1  
$EndComp
Text Label 9600 8000 3    50   ~ 0
MCU_VDDIO
Text Label 6700 8000 3    50   ~ 0
MCU_VDDIO
Text HLabel 9900 5200 2    50   Input ~ 0
CAN_TX_2
Text HLabel 5800 6600 0    50   Output ~ 0
CAN_RX_1
Text HLabel 5800 5200 0    50   Output ~ 0
CAN_RX_2
Text HLabel 5800 5000 0    50   Output ~ 0
CAN_SILENT_2
Text HLabel 5800 6100 0    50   Input ~ 0
CAN_TX_1
Text Label 5800 7100 2    50   ~ 0
MCU_RTC_OUT
Text Label 5800 7000 2    50   ~ 0
MCU_RTC_IN
Text Label 6300 2700 1    50   ~ 0
MCU_CLK_IN
Text Label 8700 8300 3    50   ~ 0
SPI_MOSI
Text Label 8900 8300 3    50   ~ 0
SPI_MISO
Text Label 8400 8300 3    50   ~ 0
SPI_SCK
Text Label 9900 3800 0    50   ~ 0
MEM_NAND_WRT_PROTECT
Text Label 9900 4300 0    50   ~ 0
MEM_NAND_BUSY_1
Text Label 9900 4400 0    50   ~ 0
MEM_NAND_ADDR_ENABLE
Text Label 9900 4100 0    50   ~ 0
MEM_NAND_CMD_ENABLE
Text Label 9900 4700 0    50   ~ 0
MEM_NAND_ENABLE_1
Text Label 9900 5400 0    50   ~ 0
MEM_NAND_WRT_ENABLE
Text Label 9900 5800 0    50   ~ 0
MEM_NAND_OUT_ENABLE
Text Label 7200 8000 3    50   ~ 0
MEM_D7
Text Label 7800 8000 3    50   ~ 0
MEM_D6
Text Label 8200 8000 3    50   ~ 0
MEM_D5
Text Label 6200 8000 3    50   ~ 0
MEM_D1
Text Label 5800 4600 2    50   ~ 0
MEM_D0
Text Label 6300 8000 3    50   ~ 0
MEM_D2
Text Label 6400 8000 3    50   ~ 0
MEM_D3
Text Label 6500 8000 3    50   ~ 0
MEM_D4
Text HLabel 11800 6250 2    50   Output ~ 0
I2C_SCL_FLOW
Text HLabel 11800 6150 2    50   BiDi ~ 0
I2C_SDA_FLOW
Entry Wire Line
	11150 6250 11250 6150
Entry Wire Line
	11150 6350 11250 6250
Wire Wire Line
	11250 6150 11800 6150
Wire Wire Line
	11250 6250 11800 6250
Text Label 11350 6150 0    50   ~ 0
I2C_SDA_1
Text Label 11350 6250 0    50   ~ 0
I2C_SCL_1
Text HLabel 11800 6500 2    50   Output ~ 0
I2C_SCL_CONTROL
Text HLabel 11800 6400 2    50   BiDi ~ 0
I2C_SDA_CONTROL
Entry Wire Line
	11150 6500 11250 6400
Entry Wire Line
	11150 6600 11250 6500
Wire Wire Line
	11250 6400 11800 6400
Wire Wire Line
	11250 6500 11800 6500
Text Label 11350 6400 0    50   ~ 0
I2C_SDA_1
Text Label 11350 6500 0    50   ~ 0
I2C_SCL_1
Text HLabel 9000 8000 3    50   Input ~ 0
FLOW_PUMP_CURRENT_ALERT
Text HLabel 9900 5000 2    50   Input ~ 0
CONTROL_PUMP_CURRENT_ALERT
Text HLabel 8300 2700 1    50   Output ~ 0
lclset3
Text HLabel 13500 5650 0    50   Input ~ 0
MEM_VCC
Text HLabel 15800 5650 1    50   Input ~ 0
MEM_VCC
$Comp
L Device:C C42
U 1 1 61458449
P 13650 5750
F 0 "C42" V 13700 5575 50  0000 L CNN
F 1 "0.1u" V 13775 5550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 13688 5600 50  0001 C CNN
F 3 "~" H 13650 5750 50  0001 C CNN
	1    13650 5750
	0    1    1    0   
$EndComp
Connection ~ 13800 5750
Wire Wire Line
	13500 5750 13500 5650
Wire Wire Line
	13900 5650 13500 5650
$Comp
L Device:C C43
U 1 1 6146B957
P 15650 5750
F 0 "C43" V 15725 5800 50  0000 L CNN
F 1 "0.1u" V 15800 5800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 15688 5600 50  0001 C CNN
F 3 "~" H 15650 5750 50  0001 C CNN
	1    15650 5750
	0    1    1    0   
$EndComp
Connection ~ 15500 5750
Wire Wire Line
	15800 5750 15800 5650
Wire Wire Line
	15400 5650 15800 5650
Text HLabel 5800 3600 0    50   Output ~ 0
DAC1.1
Text HLabel 6100 2700 1    50   Output ~ 0
DAC2.1
Text HLabel 9200 2700 1    50   Output ~ 0
lclset1
Text HLabel 9900 7000 2    50   Input ~ 0
FG_VDOSE
Text HLabel 13400 4350 1    50   Input ~ 0
MEM_VCC
Text HLabel 5800 5400 0    50   Input ~ 0
ADC_MANIFOLD_1
Text HLabel 5800 5300 0    50   Input ~ 0
ADC_MANIFOLD_2
Text HLabel 5800 6000 0    50   Output ~ 0
DAC_PWM_1
Text HLabel 5800 4100 0    50   Output ~ 0
FLOW_VALVE_1_OPEN
Text HLabel 2800 7700 3    50   Output ~ 0
FLOW_PUMP_PWM_B2
Text HLabel 3100 6300 1    50   Output ~ 0
CONTROL_PUMP_PWM_B2
Text HLabel 3800 6700 2    50   Output ~ 0
CONTROL_PUMP_PWM_B1
$Comp
L power:GND #PWR?
U 1 1 6210B5C8
P 1600 7100
AR Path="/6210B5C8" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/6210B5C8" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/6210B5C8" Ref="#PWR0177"  Part="1" 
F 0 "#PWR0177" H 1600 6850 50  0001 C CNN
F 1 "GND" H 1605 6927 50  0000 C CNN
F 2 "" H 1600 7100 50  0001 C CNN
F 3 "" H 1600 7100 50  0001 C CNN
	1    1600 7100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 6210B5CF
P 1200 7000
AR Path="/6210B5CF" Ref="R?"  Part="1" 
AR Path="/5F9F1786/6210B5CF" Ref="R?"  Part="1" 
AR Path="/5FBFE149/6210B5CF" Ref="R146"  Part="1" 
F 0 "R146" H 1270 7046 50  0000 L CNN
F 1 "10k" H 1270 6955 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1130 7000 50  0001 C CNN
F 3 "~" H 1200 7000 50  0001 C CNN
	1    1200 7000
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 7200 1200 7200
Wire Wire Line
	1700 6800 1600 6800
Wire Wire Line
	1200 6800 1200 6850
$Comp
L power:GND #PWR?
U 1 1 6210B5DB
P 1700 7400
AR Path="/6210B5DB" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/6210B5DB" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/6210B5DB" Ref="#PWR0178"  Part="1" 
F 0 "#PWR0178" H 1700 7150 50  0001 C CNN
F 1 "GND" H 1705 7227 50  0000 C CNN
F 2 "" H 1700 7400 50  0001 C CNN
F 3 "" H 1700 7400 50  0001 C CNN
	1    1700 7400
	0    1    1    0   
$EndComp
NoConn ~ 3800 7000
NoConn ~ 3800 7100
$Comp
L power:GND #PWR?
U 1 1 6210B5E4
P 2600 7700
AR Path="/6210B5E4" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/6210B5E4" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/6210B5E4" Ref="#PWR0179"  Part="1" 
F 0 "#PWR0179" H 2600 7450 50  0001 C CNN
F 1 "GND" H 2605 7527 50  0000 C CNN
F 2 "" H 2600 7700 50  0001 C CNN
F 3 "" H 2600 7700 50  0001 C CNN
	1    2600 7700
	1    0    0    -1  
$EndComp
NoConn ~ 1700 6900
NoConn ~ 1700 7000
Wire Notes Line
	2250 5900 780  5900
Text Notes 890  5990 0    50   ~ 0
Leave XERR unconnected(?)
Text Notes 830  6320 0    50   ~ 0
If you use 5v rail sue resistor \ndividers for outputs to MCU\n- leave blank pull-up unconnected\n- leave xerr pull-up unconnected
Wire Notes Line
	750  8950 4850 8950
Wire Notes Line
	4850 8950 4850 5500
Wire Notes Line
	4850 5500 750  5500
Wire Notes Line
	750  5500 750  8950
$Comp
L T1M-10-GF-DV:T1M-10-GF-DV J20
U 1 1 620A9B85
P 1815 950
F 0 "J20" H 1900 1035 50  0000 C CNN
F 1 "T1M-10-GF-DV" H 2570 965 50  0000 C CNN
F 2 "SU_connectors:T1M10GFDV" H 2565 1050 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/FTSH-110-05-F-DV.pdf" H 2565 950 50  0001 L CNN
F 4 "20 Position, High Reliability Header Strips, 0.050&quot; pitch" H 2565 850 50  0001 L CNN "Description"
F 5 "" H 2565 750 50  0001 L CNN "Height"
F 6 "200-FTSH11005FDV" H 2565 650 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=200-FTSH11005FDV" H 2565 550 50  0001 L CNN "Mouser Price/Stock"
F 8 "SAMTEC" H 2565 450 50  0001 L CNN "Manufacturer_Name"
F 9 "FTSH-110-05-F-DV" H 2565 350 50  0001 L CNN "Manufacturer_Part_Number"
	1    1815 950 
	0    1    1    0   
$EndComp
Text HLabel 3100 7700 3    50   Output ~ 0
CONTROL_VALVE_3_SIGNAL
Text HLabel 3800 7200 2    50   Output ~ 0
CONTROL_VALVE_4_SIGNAL
Text HLabel 3800 7400 2    50   Output ~ 0
CONTROL_VALVE_1_SIGNAL
Text HLabel 3800 6800 2    50   Output ~ 0
CONTROL_VALVE_6_SIGNAL
Text HLabel 3800 7300 2    50   Output ~ 0
CONTROL_VALVE_5_SIGNAL
Text HLabel 3800 6900 2    50   Output ~ 0
CONTROL_VALVE_2_SIGNAL
NoConn ~ 2800 6300
NoConn ~ 2700 6300
NoConn ~ 2500 6300
NoConn ~ 8300 8000
Text Notes 7700 2400 1    50   ~ 0
PWM128
Wire Notes Line
	2250 5900 2250 6370
Wire Notes Line
	790  5900 790  6370
Wire Notes Line
	790  6370 2250 6370
$Comp
L ECS-2520MVLC-120-CN-TR:ECS-2520MVLC-120-CN-TR Y3
U 1 1 622AC601
P 1480 4430
F 0 "Y3" H 2230 4695 50  0000 C CNN
F 1 "ECS-2520MVLC-120-CN-TR" H 2230 4604 50  0000 C CNN
F 2 "Components:ECS2520MVLC120CNTR" H 2830 4530 50  0001 L CNN
F 3 "https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwi43cyW_bj2AhVBXBoKHeoCBjUQFnoECBUQAQ&url=https%3A%2F%2Fecsxtal.com%2Fstore%2Fpdf%2FECS-2520MVLC.pdf&usg=AOvVaw1FVHdgV02YD-i1ba9jFZ6a" H 2830 4430 50  0001 L CNN
F 4 "Standard Clock Oscillators 12.000MHz HCMOS Supply Voltage 1.6 3.6 +/-25 ppm -40 +85C" H 2830 4330 50  0001 L CNN "Description"
F 5 "0.9" H 2830 4230 50  0001 L CNN "Height"
F 6 "" H 2830 4130 50  0001 L CNN "Mouser Part Number"
F 7 "" H 2830 4030 50  0001 L CNN "Mouser Price/Stock"
F 8 "ECS" H 2830 3930 50  0001 L CNN "Manufacturer_Name"
F 9 "ECS-2520MVLC-120-CN-TR" H 2830 3830 50  0001 L CNN "Manufacturer_Part_Number"
	1    1480 4430
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 622AE7D6
P 1300 4620
AR Path="/622AE7D6" Ref="C?"  Part="1" 
AR Path="/5FBFE149/622AE7D6" Ref="C49"  Part="1" 
F 0 "C49" H 1415 4666 50  0000 L CNN
F 1 "15pF" H 1415 4575 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1338 4470 50  0001 C CNN
F 3 "~" H 1300 4620 50  0001 C CNN
	1    1300 4620
	1    0    0    -1  
$EndComp
Wire Wire Line
	13820 7810 13820 7770
Wire Wire Line
	13820 7770 13850 7770
Wire Wire Line
	1480 4430 1300 4430
Wire Wire Line
	1300 4430 1300 4470
Wire Wire Line
	1480 4530 1480 4430
Connection ~ 1480 4430
Text GLabel 1480 4360 1    50   Input ~ 0
3.3V
Wire Wire Line
	1480 4360 1480 4430
$Comp
L power:GND #PWR?
U 1 1 622FD491
P 1300 4770
AR Path="/622FD491" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/622FD491" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/622FD491" Ref="#PWR0181"  Part="1" 
F 0 "#PWR0181" H 1300 4520 50  0001 C CNN
F 1 "GND" H 1305 4597 50  0000 C CNN
F 2 "" H 1300 4770 50  0001 C CNN
F 3 "" H 1300 4770 50  0001 C CNN
	1    1300 4770
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6230368F
P 2980 4770
AR Path="/6230368F" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/6230368F" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/6230368F" Ref="#PWR0182"  Part="1" 
F 0 "#PWR0182" H 2980 4520 50  0001 C CNN
F 1 "GND" H 2985 4597 50  0000 C CNN
F 2 "" H 2980 4770 50  0001 C CNN
F 3 "" H 2980 4770 50  0001 C CNN
	1    2980 4770
	1    0    0    -1  
$EndComp
Wire Wire Line
	2980 4530 2980 4770
Wire Wire Line
	3210 4430 2980 4430
Text Label 3210 4430 0    50   ~ 0
TLC_PWM_EXTRNL
Text Label 2400 6300 1    50   ~ 0
TLC_PWM_EXTRNL
Wire Notes Line
	750  5120 3880 5120
Wire Notes Line
	3880 5120 3880 3950
Wire Notes Line
	3880 3950 750  3950
Wire Notes Line
	750  3950 750  5120
Text Notes 1730 3950 0    50   ~ 0
TLC EXTERNAL PWM SIGNAL
Text Label 1200 7200 2    50   ~ 0
TLC_BLANK
Text Label 2500 7700 3    50   ~ 0
SPI_MOSI
Text Label 2400 7700 3    50   ~ 0
SPI_SCK
Text Label 1200 7300 2    50   ~ 0
SPI_NSS_3
Text Label 6100 8000 3    50   ~ 0
TLC_BLANK
Text HLabel 7600 8000 3    50   Output ~ 0
SPI_NSS_1
NoConn ~ 9500 8000
Text Notes 7650 8700 1    50   ~ 0
NCS1
Text Notes 7150 8700 1    50   ~ 0
NCS3
Text Notes 8700 2200 1    50   ~ 0
GPIO for NCS2
Text Notes 12250 1650 0    50   ~ 0
GENERAL REMARKS\n-SPI0\n-I2C1 TOP RIGHT\n-I2C0 MIDDLE RIGHT\n- pull-down both enable pins on NAND (software consideration)\n- LED SIGNALS CURRENTLY ON GPIOs
NoConn ~ 9900 4500
Text Notes 9200 8250 1    50   ~ 0
PWM68\n
NoConn ~ 9200 8000
Text Notes 7500 8300 1    50   ~ 0
PWM51\n
NoConn ~ 5800 5900
Text Notes 6600 8300 1    50   ~ 0
PWM42\n
NoConn ~ 5800 5600
Text HLabel 5800 5100 0    50   Input ~ 0
CAN_FAULT_2
Text Notes 8000 2500 1    50   ~ 0
PWM125
Text Notes 8100 2500 1    50   ~ 0
PWM1_LOW_124
Text Label 8200 2700 1    50   ~ 0
MCU_VDDPLL
Text Notes 8400 2500 1    50   ~ 0
PWM1_LOW_121
Text Notes 8500 2500 1    50   ~ 0
PWM1_LOW_120
NoConn ~ 8500 2700
Text Notes 8600 2450 1    50   ~ 0
PWM119
Text Notes 8800 2300 1    50   ~ 0
PWM117
Text Notes 8900 2300 1    50   ~ 0
PWM_LOW_116
NoConn ~ 9900 3600
Text Notes 10050 4200 0    50   ~ 0
PWM102
NoConn ~ 9900 5100
Text Notes 10050 5150 0    50   ~ 0
PWM93
Text Notes 10050 4500 0    50   ~ 0
PWM_LOW_99
Text Notes 8300 2500 1    50   ~ 0
PWM122
Text Notes 4900 6050 0    39   Italic 8
PWM0-PWMH3
Text Notes 10700 5150 0    39   Italic 8
PWM0-PWMH1
Text Notes 10700 4200 0    39   Italic 8
PWM0-PWMH0
Text Notes 7325 1745 1    39   Italic 8
PWM1-PWMH0
NoConn ~ 7500 8000
Text Label 7100 8000 3    50   ~ 0
SPI_NSS_3
Wire Wire Line
	1700 7300 1400 7300
Wire Wire Line
	1200 7150 1200 7200
Connection ~ 1400 7300
Wire Wire Line
	1400 7300 1200 7300
Wire Wire Line
	1400 6850 1400 6800
Connection ~ 1400 6800
Wire Wire Line
	1400 6800 1200 6800
Text Notes 7500 8750 1    39   Italic 8
PWM1-PWMH1
Text Notes 7700 2050 1    39   Italic 8
PWM1-PWMH1
Text Notes 8000 2100 1    39   Italic 8
PWMC1_PWMH2
Text Notes 8300 2150 1    39   Italic 8
PWM0-PWMH3
Text Notes 8600 2050 1    39   Italic 8
PWM1-PWMH3
Text Notes 8800 1950 1    39   Italic 8
PWM0-PWMH2
Text Notes 9250 8700 1    39   Italic 8
PWM1-PWMH0
Text Notes 4850 7100 0    39   Italic 8
PWM1-PWMH3
Text Notes 8800 8650 1    39   Italic 8
PWM0-PWMH0
Text Notes 9250 9200 1    39   Italic 8
PWM0-PWMH1
Text Notes 6600 8750 1    39   Italic 8
PWM0-PWMH2
Text Notes 7500 9200 1    39   Italic 8
PWM0-PWMH3*
Text Notes 4850 5800 0    39   Italic 8
PWM0-PWMH0
Text HLabel 5800 5800 0    50   Input ~ 0
CAN_FAULT_1
Text HLabel 5800 5700 0    50   Output ~ 0
CAN_SILENT_1
Text HLabel 5800 4800 0    50   Output ~ 0
FLOW_VALVE_3_OPEN
Text HLabel 5800 5500 0    50   Output ~ 0
FLOW_VALVE_3_CLOSE
Text Notes 7500 1800 3    50   ~ 0
change camera rst
Text HLabel 7500 2700 1    50   Output ~ 0
USB_EN
Text HLabel 7400 2700 1    50   Output ~ 0
USB_FAULT
Text HLabel 9100 2700 1    50   Output ~ 0
HEATER_CONTROL_1
Text HLabel 7700 2700 1    50   Output ~ 0
LED_1_SIGNAL
Text HLabel 8000 2700 1    50   Output ~ 0
LED_2_SIGNAL
Text HLabel 8600 2700 1    50   Output ~ 0
LED_3_SIGNAL
Text Notes 9150 8500 1    50   ~ 0
led\n
Text Notes 7300 8250 1    50   ~ 0
led\n
Text Notes 6900 8200 1    50   ~ 0
led\n
Text Notes 9450 8350 1    50   ~ 0
led\n
Text Label 8100 8000 3    50   ~ 0
SPARE_GPIO_3
Text Label 8000 8000 3    50   ~ 0
SPARE_GPIO_2
Text Label 7900 8000 3    50   ~ 0
SPARE_GPIO_1
Text Label 8100 2700 1    50   ~ 0
DBG_LED
NoConn ~ 9100 8000
NoConn ~ 7200 2700
NoConn ~ 9400 8000
NoConn ~ 9900 6900
NoConn ~ 9900 7100
NoConn ~ 7700 8000
NoConn ~ 7300 8000
NoConn ~ 6600 8000
Text HLabel 9400 2700 1    50   Output ~ 0
FLOW_VALVE_5_OPEN
Text HLabel 9500 2700 1    50   Output ~ 0
FLOW_VALVE_5_CLOSE
Text HLabel 8900 2700 1    50   Output ~ 0
HEATER_CONTROL_CHIP
Text HLabel 5800 6200 0    50   Input ~ 0
VALVES_TEMP_IN
NoConn ~ 5800 3900
NoConn ~ 9300 8000
NoConn ~ 6400 2700
Connection ~ 1200 6800
$Comp
L Device:R R?
U 1 1 6210B5EC
P 1550 6700
AR Path="/6210B5EC" Ref="R?"  Part="1" 
AR Path="/5F9F1786/6210B5EC" Ref="R?"  Part="1" 
AR Path="/5FBFE149/6210B5EC" Ref="R148"  Part="1" 
F 0 "R148" V 1450 6600 50  0000 L CNN
F 1 "2.2K" V 1550 6600 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1480 6700 50  0001 C CNN
F 3 "~" H 1550 6700 50  0001 C CNN
	1    1550 6700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1200 6600 1200 6800
Wire Wire Line
	1200 6600 1700 6600
$Comp
L power:GND #PWR?
U 1 1 62443340
P 9900 5900
AR Path="/62443340" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/62443340" Ref="#PWR0155"  Part="1" 
F 0 "#PWR0155" H 9900 5650 50  0001 C CNN
F 1 "GND" H 9905 5727 50  0000 C CNN
F 2 "" H 9900 5900 50  0001 C CNN
F 3 "" H 9900 5900 50  0001 C CNN
	1    9900 5900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C51
U 1 1 6251C92C
P 1600 6950
F 0 "C51" H 1715 6996 50  0000 L CNN
F 1 "0.1u" H 1715 6905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1638 6800 50  0001 C CNN
F 3 "~" H 1600 6950 50  0001 C CNN
	1    1600 6950
	1    0    0    -1  
$EndComp
Connection ~ 1600 6800
Wire Wire Line
	1600 6800 1400 6800
Wire Wire Line
	1700 7100 1600 7100
Connection ~ 1600 7100
NoConn ~ 2600 6300
Wire Wire Line
	15050 7770 15250 7770
NoConn ~ 7300 2700
Text Notes 7335 2650 1    50   ~ 0
AVAILABLE PWM CHANNEL
NoConn ~ 1815 950 
NoConn ~ 7600 2700
NoConn ~ 7800 2700
NoConn ~ 7900 2700
NoConn ~ 9300 2700
NoConn ~ 5800 4900
$Comp
L Components:ATSAMV71Q21B-AAB IC?
U 1 1 5FC255E5
P 5800 3600
AR Path="/5FC255E5" Ref="IC?"  Part="1" 
AR Path="/5FBFE149/5FC255E5" Ref="IC10"  Part="1" 
F 0 "IC10" H 7700 1950 50  0000 L CNN
F 1 "ATSAMV71Q21B-AAB" H 7400 1850 50  0000 L CNN
F 2 "Components:QFP50P2200X2200X160-144N" H 9750 4300 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-44003-32-bit-Cortex-M7-Microcontroller-SAM-V71Q-SAM-V71N-SAM-V71J_Datasheet.pdf" H 9750 4200 50  0001 L CNN
F 4 "ARM Microcontrollers - MCU 2MB FLASH 384KB SRAM LQFP144" H 9750 4100 50  0001 L CNN "Description"
F 5 "1.6" H 9750 4000 50  0001 L CNN "Height"
F 6 "579-ATSAMV71Q21B-AAB" H 9750 3900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Microchip-Technology-Atmel/ATSAMV71Q21B-AAB?qs=j%252B1pi9TdxUaaYXmCSj2x3Q%3D%3D" H 9750 3800 50  0001 L CNN "Mouser Price/Stock"
F 8 "Microchip" H 9750 3700 50  0001 L CNN "Manufacturer_Name"
F 9 "ATSAMV71Q21B-AAB" H 9750 3600 50  0001 L CNN "Manufacturer_Part_Number"
	1    5800 3600
	1    0    0    -1  
$EndComp
NoConn ~ 5800 3700
$Comp
L TLC5940RHBR:TLC5940RHBR IC?
U 1 1 6210B5BF
P 2400 7700
AR Path="/5F9F1786/6210B5BF" Ref="IC?"  Part="1" 
AR Path="/5FBFE149/6210B5BF" Ref="IC9"  Part="1" 
F 0 "IC9" V 3050 7300 50  0000 L CNN
F 1 "TLC5940RHBR" V 3150 7100 50  0000 L CNN
F 2 "QFN50P500X500X100-33N-D" H 3650 8200 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/tlc5940.pdf" H 3650 8100 50  0001 L CNN
F 4 "16-Channel LED Driver w/EEprom DOT Correction & Grayscale PWM Control" H 3650 8000 50  0001 L CNN "Description"
F 5 "1" H 3650 7900 50  0001 L CNN "Height"
F 6 "595-TLC5940RHBR" H 3650 7800 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/TLC5940RHBR?qs=p6lVfQR1GSql0QiKiHXw6g%3D%3D" H 3650 7700 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 3650 7600 50  0001 L CNN "Manufacturer_Name"
F 9 "TLC5940RHBR" H 3650 7500 50  0001 L CNN "Manufacturer_Part_Number"
	1    2400 7700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6210B5F2
P 1400 6700
AR Path="/6210B5F2" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/6210B5F2" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/6210B5F2" Ref="#PWR0180"  Part="1" 
F 0 "#PWR0180" H 1400 6450 50  0001 C CNN
F 1 "GND" H 1405 6527 50  0000 C CNN
F 2 "" H 1400 6700 50  0001 C CNN
F 3 "" H 1400 6700 50  0001 C CNN
	1    1400 6700
	1    0    0    -1  
$EndComp
Text GLabel 1200 6800 0    50   Input ~ 0
3.3V
$Comp
L power:GND #PWR?
U 1 1 624833DB
P 1400 7600
AR Path="/624833DB" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/624833DB" Ref="#PWR?"  Part="1" 
AR Path="/5FBFE149/624833DB" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 1400 7350 50  0001 C CNN
F 1 "GND" H 1405 7427 50  0000 C CNN
F 2 "" H 1400 7600 50  0001 C CNN
F 3 "" H 1400 7600 50  0001 C CNN
	1    1400 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62482D37
P 1400 7450
AR Path="/62482D37" Ref="R?"  Part="1" 
AR Path="/5F9F1786/62482D37" Ref="R?"  Part="1" 
AR Path="/5FBFE149/62482D37" Ref="R151"  Part="1" 
F 0 "R151" H 1250 7450 50  0000 L CNN
F 1 "10k" H 1200 7350 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1330 7450 50  0001 C CNN
F 3 "~" H 1400 7450 50  0001 C CNN
	1    1400 7450
	1    0    0    -1  
$EndComp
NoConn ~ -5965 -12205
NoConn ~ 7480 2750
Text Notes 1565 9945 0    50   ~ 0
ok
Text Notes 1560 10835 0    50   ~ 0
ok
Text Notes 6010 9565 0    50   ~ 0
ok
Text Notes 7510 10710 0    50   ~ 0
ok
Wire Bus Line
	9600 2200 11150 2200
Wire Bus Line
	11150 2200 11150 3900
Wire Bus Line
	8400 8900 9070 8900
Wire Bus Line
	11150 5300 11150 6700
$EndSCHEMATC
